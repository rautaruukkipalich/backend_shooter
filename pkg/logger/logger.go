package logger

import (
	"backend_shooter/config"
	"context"
	"github.com/google/uuid"
	"io"
	"log"
	"log/slog"
	"net/http"
	"strings"
)

type (
	ctxKey string
)

var (
	level = map[string]slog.Level{
		"DEBUG": slog.LevelDebug,
		"INFO":  slog.LevelInfo,
		"WARN":  slog.LevelWarn,
		"ERROR": slog.LevelError,
	}

	ctxLogger       ctxKey = "ctxLogger"
	ctxRequestGroup ctxKey = "ctxRequestGroup"
)

func New(
	cfg *config.LoggerConfig,
	w io.Writer,
	replaceAttr func([]string, slog.Attr) slog.Attr,
) (*slog.Logger, error) {

	logLevel, ok := level[strings.ToUpper(cfg.Level)]
	if !ok {
		return nil, ErrInvalidLogLevel
	}

	var handler slog.Handler

	switch strings.ToUpper(cfg.Type) {
	case "TEXT":
		handler = slog.NewTextHandler(w, &slog.HandlerOptions{
			Level:       logLevel,
			ReplaceAttr: replaceAttr,
		})
	case "JSON":
		handler = slog.NewJSONHandler(w, &slog.HandlerOptions{
			Level:       logLevel,
			ReplaceAttr: replaceAttr,
		})
	default:
		return nil, ErrInvalidLogType
	}

	return slog.New(handler), nil
}

func FromContext(ctx context.Context) *slog.Logger {
	defer func() {
		if r := recover(); r != nil {
			log.Println("Recovered in fromContext", r)
		}
	}()

	//for tests
	l, ok := ctx.Value(ctxLogger).(*slog.Logger)
	if !ok {
		l, _ := ForTests()
		return l
	}

	requestGroup, ok := ctx.Value(ctxRequestGroup).(slog.Attr)
	if !ok {
		return l
	}
	return l.With(requestGroup)
}

func ToContext(ctx context.Context, l *slog.Logger) context.Context {
	return context.WithValue(ctx, ctxLogger, l)
}

func AddRequestInfo(ctx context.Context, r *http.Request) context.Context {
	return context.WithValue(
		ctx,
		ctxRequestGroup,
		slog.Group(
			"request",
			slog.String("path", r.RequestURI),
			slog.String("method", r.Method),
			slog.String("uuid", uuid.New().String()),
		),
	)
}

package logger

import "log/slog"

type fakeWriter struct {
}

func (logger *fakeWriter) Write(_ []byte) (n int, err error) {
	return 0, nil
}

func ForTests() (*slog.Logger, error) {
	return slog.New(slog.NewTextHandler(&fakeWriter{}, &slog.HandlerOptions{Level: slog.LevelError})), nil
}

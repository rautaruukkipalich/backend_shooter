package logger

import "errors"

var (
	ErrInvalidLogLevel = errors.New("invalid log level. use 'debug' / 'info' / 'warn' / 'error'")
	ErrInvalidLogType  = errors.New("invalid log type. use 'text' / 'json'")
)

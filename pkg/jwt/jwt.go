package jwt

import (
	"github.com/golang-jwt/jwt"
	"time"
)

type (
	JWT struct {
		secretKey  []byte
		ttl        int
		ttlSeconds time.Duration
	}

	DecodedJWT struct {
		UserID int64
		CharID int64
	}
)

const (
	ttl = 60 * 60 * 24
)

func New() *JWT {
	return &JWT{
		secretKey:  []byte("secretkey"),
		ttl:        ttl,
		ttlSeconds: ttl * time.Second,
	}
}

func (j *JWT) GetTTL() int {
	return j.ttl
}

func (j *JWT) Encode(userId, characterId int64) (string, error) {
	payload := jwt.MapClaims{
		"sub": userId,
		"cid": characterId,
		"iat": time.Now().Unix(),
		"exp": time.Now().Add(time.Duration(j.ttl) * time.Second).Unix(),
	}

	// Создаем новый JWT-токен и подписываем его по алгоритму HS512
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, payload)

	signedToken, err := token.SignedString(j.secretKey)
	if err != nil {
		return "", ErrJWTEncode
	}
	return signedToken, nil
}

func (j *JWT) Decode(token string) (DecodedJWT, error) {
	claims := jwt.MapClaims{}
	_, err := jwt.ParseWithClaims(
		token,
		claims,
		func(token *jwt.Token) (any, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, ErrJWTDecode
			}
			return j.secretKey, nil
		},
	)

	if err != nil {
		return DecodedJWT{}, err
	}

	jwtDecoded := DecodedJWT{
		UserID: int64(claims["sub"].(float64)),
		CharID: int64(claims["cid"].(float64)),
	}

	return jwtDecoded, nil
}

package jwt

import "errors"

var (
	ErrJWTDecode = errors.New("unexpected signing method")
	ErrJWTEncode = errors.New("JWT token failed to signed")
)

package config

import (
	"time"
)

type Config struct {
	Logger  LoggerConfig  `yaml:"logger" required:"true"`
	Server  ServerConfig  `yaml:"server" required:"true"`
	SQLDB   SQLDBConfig   `yaml:"sql_db" required:"true"`
	Metrics MetricsConfig `yaml:"metrics" required:"true"`
	Cors    CorsConfig    `yaml:"cors" required:"true"`
}

type LoggerConfig struct {
	Level    string `yaml:"level" required:"true"`
	Type     string `yaml:"type" required:"true"`
	Out      string `yaml:"out" required:"true"`
	Filepath string `yaml:"filepath" required:"false"`
}

type ServerConfig struct {
	Addr      string        `yaml:"addr" required:"true"`
	WSTimeout time.Duration `yaml:"ws_timeout" required:"true"`
}

type SQLDBConfig struct {
	URI            string `yaml:"uri" required:"true"`
	MaxConnections int    `yaml:"max_connections" required:"true"`
	Driver         string `yaml:"driver" required:"true"`
}

type MetricsConfig struct {
	URI string `yaml:"uri" required:"true"`
}

type CorsConfig struct {
	AllowedOrigins   []string `yaml:"allowed_origins" required:"true"`
	AllowedMethods   []string `yaml:"allowed_methods" required:"true"`
	AllowedHeaders   []string `yaml:"allowed_headers" required:"true"`
	AllowCredentials bool     `yaml:"allow_credentials" required:"true"`
}

SELECT c.id,
       c.name,
       c.user_id,
       c.health,
       p.pos_x_int,
       p.pos_x_dec,
       p.pos_y_int,
       p.pos_y_dec,
       p.pos_z_int,
       p.pos_z_dec
FROM characters AS c
         JOIN positions AS p
              ON c.id = p.character_id
WHERE c.id = $1
ORDER BY p.created_at DESC
LIMIT 1;
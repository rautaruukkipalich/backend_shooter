INSERT INTO characters(name,
                       user_id,
                       health,
                       created_at)
VALUES ($1, $2, $3, $4)
RETURNING id;
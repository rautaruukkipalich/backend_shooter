INSERT INTO positions(character_id,
                      pos_x_int,
                      pos_x_dec,
                      pos_y_int,
                      pos_y_dec,
                      pos_z_int,
                      pos_z_dec,
                      created_at)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8);
SELECT c.id,
       c.name,
       c.user_id,
       c.health
FROM characters AS c
WHERE c.user_id = $1
ORDER BY c.id ASC;
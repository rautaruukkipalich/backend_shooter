INSERT INTO users(username, hashed_password, created_at)
VALUES ($1, $2, $3)
RETURNING id;
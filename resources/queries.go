package resources

import (
	_ "embed"
)

//USERS

//go:embed user/getUserByUsername.sql
var GetUserByUsername string

//go:embed user/insertUser.sql
var InsertUser string

//go:embed user/updateUser.sql
var UpdateUser string

//CHARACTERS

//go:embed character/getCharacterByName.sql
var GetCharacterByName string

//go:embed character/getCharacterByID.sql
var GetCharacterByID string

//go:embed character/getCharactersByUserID.sql
var GetCharactersByUserID string

//go:embed character/getPositionByCharacter.sql
var GetPositionByCharacter string

//go:embed character/insertCharacter.sql
var InsertCharacter string

//go:embed character/updateCharacter.sql
var UpdateCharacter string

//go:embed character/insertPosition.sql
var InsertPosition string

.PHONY: run

run: format lint migrate_main
	SET GOEXPERIMENT=arenas
	go run ./cmd/app/main.go --config=./config/config.yaml

lint:
	golangci-lint run ./...

swagger:
	swag init -g cmd/app/main.go
	swag fmt

format:
	go fmt ./...

build:
	SET GOEXPERIMENT=arenas
	go build -o bin/app.exe ./cmd/app/main.go

test: migrate_test test_internal test_e2e migrate_test_down

test_internal:
	go test ./internal/... -v -cover count=1

test_e2e: test_e2e_auth test_e2e_char test_e2e_ws

test_e2e_auth:
	go test ./test/testAuth/... -v -cover count=1

test_e2e_char:
	go test ./test/testCharacter/... -v -cover count=1

test_e2e_ws:
	go test ./test/testWS/... -v -cover count=1

docker_up:
	docker-compose up -d
	timeout 5

docker_stop:
	docker-compose stop

bench: bench_mem bench_cpu

show_bench_cpu:
	go tool pprof -http=localhost:1234 ./benchmarks/cpu.pprof

show_bench_mem:
	go tool pprof -http=localhost:1235 ./benchmarks/mem.pprof


bench_mem:
	go test ./internal/database/arenadb/... -bench=. -benchmem -memprofile=./benchmarks/mem.pprof

bench_cpu:
	go test ./internal/database/arenadb/... -bench=. -cpuprofile=./benchmarks/cpu.pprof

bench_e2e:
	go test ./test/bench/ws_bench_test.go -bench=. -benchmem

clean:
	del bin

generate_mocks:
	go generate ./...

migrate_main:
	migrate -path migrations -database "postgres://postgres:postgres@localhost:15432/shooter?sslmode=disable" up

migrate_test:
	migrate -path migrations -database "postgres://postgres:postgres@localhost:15433/shooter_test?sslmode=disable" up

migrate_test_down:
	migrate -path migrations -database "postgres://postgres:postgres@localhost:15433/shooter_test?sslmode=disable" down -all

bomb: bomb-login bomb-error

bomb-login:
	bombardier -c 50 -d 30s -m POST  http://localhost:18080/auth/login -H 'accept: application/json' -H 'Content-Type: application/json' -f 'bombing/bomb-login.json'

bomb-error:
	bombardier -c 50 -d 30s http://localhost:18080/character

rtm:
	SET GOEXPERIMENT=arenas
	go run ./internal/database/arenadb/rtm_profile.go
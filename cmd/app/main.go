package main

import (
	"backend_shooter/internal/api/auth"
	"backend_shooter/internal/api/character"
	"backend_shooter/internal/api/metrics"
	"backend_shooter/internal/api/swag"
	"backend_shooter/internal/api/ws_pull"
	"backend_shooter/internal/database/arena_pull_slice"
	//"backend_shooter/internal/database/arena_push_updates"
	//"backend_shooter/internal/api/ws"
	//"backend_shooter/internal/api/ws_push"
	"backend_shooter/internal/database/sqldb"
	metric "backend_shooter/internal/metrics"
	mw "backend_shooter/internal/middleware"
	"backend_shooter/internal/middleware/cors"
	"backend_shooter/internal/server"
	authService "backend_shooter/internal/services/auth"
	//charService "backend_shooter/internal/services/character"
	//charServicePush "backend_shooter/internal/services/character_push_repo"
	charServicePull "backend_shooter/internal/services/character_pull_repo"
	"backend_shooter/pkg/loadconfig"
	"backend_shooter/pkg/logger"
	"github.com/gorilla/mux"
	"log"
	"os"
	"os/signal"
	"syscall"
)

//	@title			Swagger Backend Shooter API
//	@version		0.0.1
//	@description	...

// @host		localhost:18080
// @BasePath	/

// @SecurityDefinitions.apikey	AuthApiKey
// @in							header
// @name						Authorization
func main() {
	cfg := loadconfig.MustLoadConfig()

	l, err := logger.New(&cfg.Logger, os.Stdout, nil)
	if err != nil {
		log.Fatal(err)
	}

	//run metrics
	go func() {
		if err := metric.Listen(cfg.Metrics.URI); err != nil {
			log.Fatal(err)
		}
	}()

	//fastStore, err := arenadb.New()
	//if err != nil {
	//	log.Fatal(err)
	//}
	//defer fastStore.Stop()

	//fastStore, err := arena_push_updates.New()
	//if err != nil {
	//	log.Fatal(err)
	//}
	//defer fastStore.Stop()

	fastStore, err := arena_pull_slice.New()
	if err != nil {
		log.Fatal(err)
	}
	defer fastStore.Stop()

	sqlStore, err := sqldb.New(&cfg.SQLDB)
	if err != nil {
		log.Fatal(err)
	}
	defer sqlStore.Stop()

	authSrv := authService.New(sqlStore)
	//charSrv := charService.New(sqlStore, fastStore)
	//charSrv := charServicePush.New(sqlStore, fastStore)
	charSrv := charServicePull.New(sqlStore, fastStore)

	c := cors.New(&cfg.Cors)

	r := mux.NewRouter()
	r.Use(c.Handler)

	mw.AddLogger(l)
	r.Use(mw.Logger)
	r.Use(mw.JWT)

	swagRouter := r.PathPrefix("/api").Subrouter()
	swag.RegisterRouter(swagRouter)

	metricsRouter := r.PathPrefix("/metrics").Subrouter()
	metrics.RegisterRouter(metricsRouter)

	authRouter := r.PathPrefix("/auth").Subrouter()
	auth.RegisterRouter(authSrv, authRouter)
	authRouter.Use(mw.Metrics)

	charRouter := r.PathPrefix("/character").Subrouter()
	character.RegisterRouter(charSrv, charRouter)
	charRouter.Use(mw.Metrics)
	charRouter.Use(mw.Auth)

	//wsRouter := r.PathPrefix("/ws").Subrouter()
	//ws.RegisterRouter(charSrv, wsRouter)
	//wsRouter.Use(mw.WSMetrics)
	//wsRouter.Use(mw.Auth)

	//wsPushRouter := r.PathPrefix("/ws").Subrouter()
	//ws_push.RegisterRouter(charSrv, wsPushRouter)
	//wsPushRouter.Use(mw.WSMetrics)
	//wsPushRouter.Use(mw.Auth)

	wsPullRouter := r.PathPrefix("/ws").Subrouter()
	ws_pull.RegisterRouter(charSrv, wsPullRouter)
	wsPullRouter.Use(mw.WSMetrics)
	wsPullRouter.Use(mw.Auth)

	l.Info("starting server")

	srv := server.New(&cfg.Server, r)
	go srv.MustRun()

	defer func() {
		srv.Stop()
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGTERM, syscall.SIGINT, os.Interrupt)

	<-stop

	l.Info("Shutting down app")
}

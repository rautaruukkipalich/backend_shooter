CREATE TABLE IF NOT EXISTS users
(
    id              BIGSERIAL                   NOT NULL PRIMARY KEY,
    username        VARCHAR                     NOT NULL UNIQUE,
    hashed_password VARCHAR                     NOT NULL,
    created_at      TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE TABLE IF NOT EXISTS characters
(
    id         BIGSERIAL                   NOT NULL PRIMARY KEY,
    name       VARCHAR                     NOT NULL UNIQUE,
    user_id    INT REFERENCES users (id)   NOT NULL,
    health     INTEGER                     NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL
);

CREATE TABLE IF NOT EXISTS positions
(
    id           BIGSERIAL                      NOT NULL PRIMARY KEY,
    character_id INT REFERENCES characters (id) NOT NULL,
    pos_x_int    INT                            NOT NULL,
    pos_x_dec    INT                            NOT NULL,
    pos_y_int    INT                            NOT NULL,
    pos_y_dec    INT                            NOT NULL,
    pos_z_int    INT                            NOT NULL,
    pos_z_dec    INT                            NOT NULL,
    created_at   TIMESTAMP WITHOUT TIME ZONE    NOT NULL
);


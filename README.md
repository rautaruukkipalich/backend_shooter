# backend_shooter

This is a backend for online game using WS, go/x/arena

Auth: register, login
Char: create cahracter, show all characters, choose character
Game: WebSocket connection

For authentication useing JWT token

Goal is to using alternative data storage for reduce GCs

```
===rtm arena get all 1000000===
Mallocs:  2_012_314
Frees:  2_006_829
LiveObjects:  5_485
PauseTotalNs:  331_962_300
NumGC:  12_026
HeapObjects:  5_485
HeapAlloc:  5_893_688
```

```
===rtm non arena get all 1000000===
Mallocs:  6_020_793
Frees:  6_015_239
LiveObjects:  5_554
PauseTotalNs:  1_277_761_000
NumGC:  54_177
HeapObjects:  5_554
HeapAlloc:  3_520_096
```

Using arenas I reached reducing:
1) Mallocs: 3 times
2) TotalNsGC: 4 times
3) TotalGCRuns: 4 times



## MAKEFILE CMDs
```bash
make run
```
```bash
make test
```
```bash
make bench
```
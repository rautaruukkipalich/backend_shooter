package testWS

import (
	"backend_shooter/internal/dto"
	ch "backend_shooter/internal/model/character"
	"backend_shooter/test/testServer"
	"context"
	"github.com/gorilla/websocket"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

type moveRequest struct {
	Action ch.Action
	Move   ch.Position
}

type playerForm struct {
	userForm dto.LoginRequest
	charForm dto.CreateCharacterRequest
}

var (
	player1 = &playerForm{
		userForm: dto.LoginRequest{
			Username: "wsUserFirst",
			Password: "wsUserFirst",
		},
		charForm: dto.CreateCharacterRequest{
			Name: "wsChar1",
		},
	}
	player2 = &playerForm{
		userForm: dto.LoginRequest{
			Username: "wsUserSecond",
			Password: "wsUserSecond",
		},
		charForm: dto.CreateCharacterRequest{
			Name: "wsChar2",
		},
	}
)

func Test_WS(t *testing.T) {
	go testServer.SRV.MustRun()
	defer testServer.SRV.Stop()
	time.Sleep(1 * time.Second) //wait for prepare

	c := testServer.NewClient()
	ctx := context.Background()
	_ = c.Register(ctx, player1.userForm)
	_ = c.Register(ctx, player2.userForm)
	conn1, teardown1 := c.ConnectWS(ctx, player1.userForm, player1.charForm)
	conn2, teardown2 := c.ConnectWS(ctx, player2.userForm, player2.charForm)

	//skip initial msg
	_, _, _ = conn1.ReadMessage()
	_, _, _ = conn2.ReadMessage()

	_ = conn1.WriteMessage(websocket.TextMessage, []byte("123"))

	_, bytes, err := conn1.ReadMessage()
	if err != nil {
		t.Fatal(err)
	}
	var resp1 dto.WSCharacterActionResponse
	err = jsoniter.Unmarshal(bytes, &resp1)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, ch.Connect, resp1.Action)

	defer teardown1()
	defer teardown2()

	var tcsWS = []struct {
		name  string
		move1 moveRequest
		move2 moveRequest
	}{
		{
			name: "on move expect no error",
			move1: moveRequest{
				Action: ch.MoveAction,
				Move:   ch.Position{X: ch.Point{Int: 1, Dec: 1}},
			},
			move2: moveRequest{
				Action: ch.MoveAction,
				Move:   ch.Position{X: ch.Point{Int: 2, Dec: 2}},
			},
		},
		{
			name: "on moveto expect no error",
			move1: moveRequest{
				Action: ch.MoveToAction,
				Move:   ch.Position{X: ch.Point{Int: 12, Dec: 0}},
			},
			move2: moveRequest{
				Action: ch.MoveToAction,
				Move:   ch.Position{X: ch.Point{Int: 21, Dec: 0}},
			},
		},
	}

	for _, tc := range tcsWS {
		t.Run(tc.name, func(t *testing.T) {
			bytes1, _ := jsoniter.Marshal(tc.move1)
			bytes2, _ := jsoniter.Marshal(tc.move2)

			if err := conn1.WriteMessage(websocket.TextMessage, bytes1); err != nil {
				t.Fatal(err)
			}

			if err := conn2.WriteMessage(websocket.TextMessage, bytes2); err != nil {
				t.Fatal(err)
			}

			_, data1, err := conn1.ReadMessage()
			if err != nil {
				t.Fatal(err)
			}
			var resp1 dto.WSCharacterActionResponse
			err = jsoniter.Unmarshal(data1, &resp1)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, tc.move2.Action, resp1.Action)
			assert.Equal(t, tc.move2.Move, resp1.Position)

			_, bytes2, err = conn2.ReadMessage()
			if err != nil {
				t.Fatal(err)
			}
			var resp2 dto.WSCharacterActionResponse
			err = jsoniter.Unmarshal(bytes2, &resp2)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, tc.move1.Action, resp2.Action)
			assert.Equal(t, tc.move1.Move, resp2.Position)
		})
	}
}

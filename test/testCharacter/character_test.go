package testCharacter

import (
	"backend_shooter/internal/dto"
	"backend_shooter/test/testServer"
	"context"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"log"
	"net/http"
	"testing"
	"time"
)

var validUserLogin = dto.LoginRequest{
	Username: "testUser",
	Password: "testPassword",
}

func Test_Character_Create(t *testing.T) {

	type characterCreate struct {
		name string
		form dto.CreateCharacterRequest
		code int
	}

	tcsCreateCharacterValidTests := []characterCreate{
		{
			name: "on correct createCharacter form expected no error",
			form: dto.CreateCharacterRequest{
				Name: "testChar",
			},
			code: http.StatusCreated,
		},
		{
			name: "on correct createCharacter form expected no error",
			form: dto.CreateCharacterRequest{
				Name: "testChar2",
			},
			code: http.StatusCreated,
		},
	}

	time.Sleep(1 * time.Second) //wait for user create

	c := testServer.NewClient()
	ctx := context.Background()

	for _, tc := range tcsCreateCharacterValidTests {
		t.Run(tc.name, func(t *testing.T) {

			w := c.Login(ctx, validUserLogin)
			var token dto.TokenResponse
			if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
				log.Fatal(err)
			}

			w = c.CreateCharacter(ctx, token.Token, tc.form)
			assert.Equal(t, tc.code, w.Code)
		})
	}

	type characterCreateInvalid struct {
		name string
		form any
		code int
	}

	tcsCreateCharacterInvalidTests := []characterCreateInvalid{
		{
			name: "on incorrect data in createCharacter form expected Bad Request",
			form: dto.CreateCharacterRequest{
				Name: "",
			},
			code: http.StatusBadRequest,
		},
		{
			name: "on duplicate character name expected Bad Request",
			form: dto.CreateCharacterRequest{
				Name: "testChar",
			},
			code: http.StatusBadRequest,
		},
		{
			name: "on incorrect form expect Bad Request",
			form: "123",
			code: http.StatusBadRequest,
		},
	}

	for _, tc := range tcsCreateCharacterInvalidTests {
		t.Run(tc.name, func(t *testing.T) {
			w := c.Login(ctx, validUserLogin)
			var token dto.TokenResponse
			if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
				log.Fatal(err)
			}

			w = c.CreateCharacter(ctx, token.Token, tc.form)
			assert.Equal(t, tc.code, w.Code)
		})
	}

	tcsCreateCharacterInvalidTests2 := []characterCreateInvalid{
		{
			name: "on empty Authorization header expected Bad Request",
			form: dto.CreateCharacterRequest{
				Name: "",
			},
			code: http.StatusUnauthorized,
		},
	}

	for _, tc := range tcsCreateCharacterInvalidTests2 {
		t.Run(tc.name, func(t *testing.T) {

			w := c.Login(ctx, validUserLogin)
			var token dto.TokenResponse
			if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
				log.Fatal(err)
			}

			w = c.CreateCharacter(ctx, "", tc.form)
			assert.Equal(t, tc.code, w.Code)
		})
	}
}

func Test_Character_GetCharacters(t *testing.T) {

	type getCharacters struct {
		name   string
		code   int
		length int
	}

	var tcsCharacterGet = []getCharacters{
		{
			name:   "on valid authorization header expect no error",
			code:   http.StatusOK,
			length: 2,
		},
	}

	time.Sleep(1 * time.Second) //wait for user create
	c := testServer.NewClient()
	ctx := context.Background()

	for _, tc := range tcsCharacterGet {
		t.Run(tc.name, func(t *testing.T) {
			w := c.Login(ctx, validUserLogin)
			var token dto.TokenResponse
			if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
				log.Fatal(err)
			}

			w = c.GetCharacters(ctx, token.Token)

			assert.Equal(t, tc.code, w.Code)

			var chars dto.CharactersResponse
			if err := jsoniter.NewDecoder(w.Body).Decode(&chars); err != nil {
				log.Fatal(err)
			}

			assert.NotEmpty(t, chars.Characters)
			assert.Len(t, chars.Characters, tc.length)
		})
	}
}

func Test_Character_ChooseCharacter(t *testing.T) {

	type chooseCharacter struct {
		name string
		code int
	}

	var tcsCharacterGet = []chooseCharacter{
		{
			name: "on valid authorization header and valid charID expect no error",
			code: http.StatusOK,
		},
	}

	time.Sleep(1 * time.Second) //wait for user create
	c := testServer.NewClient()
	ctx := context.Background()

	for _, tc := range tcsCharacterGet {
		t.Run(tc.name, func(t *testing.T) {
			w := c.Login(ctx, validUserLogin)
			var token dto.TokenResponse
			if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
				log.Fatal(err)
			}

			w = c.GetCharacters(ctx, token.Token)
			assert.Equal(t, tc.code, w.Code)

			var chars dto.CharactersResponse
			if err := jsoniter.NewDecoder(w.Body).Decode(&chars); err != nil {
				log.Fatal(err)
			}

			charID := int(chars.Characters[0].ID)

			w = c.ChooseCharacter(ctx, token.Token, charID)
			assert.Equal(t, tc.code, w.Code)

			if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
				log.Fatal(err)
			}

			assert.GreaterOrEqual(t, len(token.Token), 60)
		})
	}
}

package testAuth

import (
	"backend_shooter/internal/dto"
	"backend_shooter/test/testServer"
	"context"
	jsoniter "github.com/json-iterator/go"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func Test_Register(t *testing.T) {
	var tcs = []struct {
		name    string
		regData dto.LoginRequest
		code    int
	}{
		{
			name: "on correct username and password expected no error",
			regData: dto.LoginRequest{
				Username: "testUser",
				Password: "testPassword",
			},
			code: http.StatusCreated,
		},
		{
			name: "on incorrect username expected BadRequest",
			regData: dto.LoginRequest{
				Username: "testUser321",
				Password: "testPassword",
			},
			code: http.StatusBadRequest,
		},
		{
			name: "on incorrect password expected BadRequest",
			regData: dto.LoginRequest{
				Username: "testUser",
				Password: " ",
			},
			code: http.StatusBadRequest,
		},
	}

	c := testServer.NewClient()
	ctx := context.Background()

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			w := c.Register(ctx, tc.regData)
			assert.Equal(t, tc.code, w.Code)
		})
	}
}

func Test_Login(t *testing.T) {

	var tcs = []struct {
		name    string
		regData dto.LoginRequest
		code    int
	}{
		{
			name: "on correct username and password expected no error",
			regData: dto.LoginRequest{
				Username: "testUser",
				Password: "testPassword",
			},
			code: http.StatusOK,
		},
		{
			name: "on incorrect username expected BadRequest",
			regData: dto.LoginRequest{
				Username: "testUser321",
				Password: "testPassword",
			},
			code: http.StatusBadRequest,
		},
		{
			name: "on incorrect password expected BadRequest",
			regData: dto.LoginRequest{
				Username: "testUser",
				Password: " ",
			},
			code: http.StatusBadRequest,
		},
	}

	c := testServer.NewClient()
	ctx := context.Background()

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {

			w := c.Login(ctx, tc.regData)
			assert.Equal(t, tc.code, w.Code)

			if tc.code == http.StatusOK {
				var token dto.TokenResponse
				err := jsoniter.NewDecoder(w.Body).Decode(&token)
				if err != nil {
					t.Fatal(err)
				}
				assert.NotNil(t, token)
			}
		})
	}
}

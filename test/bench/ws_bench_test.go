package bench

import (
	"backend_shooter/internal/dto"
	ch "backend_shooter/internal/model/character"
	"backend_shooter/test/testServer"
	"context"
	"fmt"
	"github.com/gorilla/websocket"
	jsoniter "github.com/json-iterator/go"
	"io"
	"log"
	"os"
	"runtime"
	"sync/atomic"
	"testing"
	"time"
)

type moveRequest struct {
	Action ch.Action
	Move   ch.Position
}

type playerForm struct {
	userForm dto.LoginRequest
	charForm dto.CreateCharacterRequest
}

var (
	player1 = &playerForm{
		userForm: dto.LoginRequest{
			Username: "wsUserFirst",
			Password: "wsUserFirst",
		},
		charForm: dto.CreateCharacterRequest{
			Name: "wsChar1",
		},
	}
	player2 = &playerForm{
		userForm: dto.LoginRequest{
			Username: "wsUserSecond",
			Password: "wsUserSecond",
		},
		charForm: dto.CreateCharacterRequest{
			Name: "wsChar2",
		},
	}
	player3 = &playerForm{
		userForm: dto.LoginRequest{
			Username: "wsUserThird",
			Password: "wsUserThird",
		},
		charForm: dto.CreateCharacterRequest{
			Name: "wsChar3",
		},
	}
	player4 = &playerForm{
		userForm: dto.LoginRequest{
			Username: "wsUserForth",
			Password: "wsUserForth",
		},
		charForm: dto.CreateCharacterRequest{
			Name: "wsChar4",
		},
	}
)

func Benchmark_WS(b *testing.B) {
	go testServer.SRV.MustRun()
	defer testServer.SRV.Stop()
	time.Sleep(1 * time.Second) //wait for prepare

	c := testServer.NewClient()
	ctx := context.Background()
	_ = c.Register(ctx, player1.userForm)
	_ = c.Register(ctx, player2.userForm)
	_ = c.Register(ctx, player3.userForm)
	_ = c.Register(ctx, player4.userForm)

	conn1, teardown1 := c.ConnectWS(ctx, player1.userForm, player1.charForm)
	conn2, teardown2 := c.ConnectWS(ctx, player2.userForm, player2.charForm)
	conn3, teardown3 := c.ConnectWS(ctx, player3.userForm, player3.charForm)
	conn4, teardown4 := c.ConnectWS(ctx, player4.userForm, player4.charForm)

	conns := []*websocket.Conn{
		conn1,
		conn2,
		conn3,
		conn4,
	}

	length := len(conns)

	//skip initial msg
	for i := 0; i < length; i++ {
		for j := 0; j < length-i; j++ {
			_, _, _ = conns[i].ReadMessage()
		}
	}

	defer teardown1()
	defer teardown2()
	defer teardown3()
	defer teardown4()
	var readCounter int32
	var writeCounter int32

	msg, _ := jsoniter.Marshal(
		moveRequest{
			Action: ch.MoveToAction,
			Move:   ch.Position{X: ch.Point{Int: 1, Dec: 0}},
		},
	)

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	wsReadLoop := func(ctx context.Context, conn *websocket.Conn) {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				_, _, err := conn.ReadMessage()
				if err != nil {
					return
				}
				atomic.AddInt32(&readCounter, 1)
			}
		}
	}

	for _, conn := range conns[1:] {
		go wsReadLoop(ctx, conn)
	}

	start := time.Now()
	b.Run("bench", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for j := 0; j < i; j++ {
				_ = conn1.WriteMessage(websocket.TextMessage, msg)
				atomic.AddInt32(&writeCounter, 1)
			}
		}
	})

	result(time.Since(start), readCounter, writeCounter, int32(len(conns)-1))
	time.Sleep(100 * time.Millisecond)
	//printMemStats(os.Stdout, "msgSend", rtm)
}

func Test_RPS(t *testing.T) {
	if os.Getenv("SKIPTEST") == "" {
		t.Skip("skip test")
	}

	srvs := []struct {
		name string
		srv  *testServer.TestingServer
	}{
		//{
		//	"srv",
		//	testServer.SRV,
		//},
		//{
		//	"srv_push",
		//	testServer.SRVpush,
		//},
		{
			"srv_pull",
			testServer.SRVpull,
		},
	}

	for _, srv := range srvs {
		server := srv
		go server.srv.MustRun()
		t.Run(server.name, func(t *testing.T) {
			testRPS()
		})
		server.srv.Stop()
	}
}

func testRPS() {
	var rtm runtime.MemStats
	time.Sleep(1 * time.Second) //wait for prepare server

	c := testServer.NewClient()
	ctx := context.Background()
	_ = c.Register(ctx, player1.userForm)
	_ = c.Register(ctx, player2.userForm)
	_ = c.Register(ctx, player3.userForm)
	_ = c.Register(ctx, player4.userForm)

	conn1, teardown1 := c.ConnectWS(ctx, player1.userForm, player1.charForm)
	conn2, teardown2 := c.ConnectWS(ctx, player2.userForm, player2.charForm)
	conn3, teardown3 := c.ConnectWS(ctx, player3.userForm, player3.charForm)
	conn4, teardown4 := c.ConnectWS(ctx, player4.userForm, player4.charForm)

	conns := []*websocket.Conn{
		conn1,
		conn2,
		conn3,
		conn4,
	}

	length := len(conns)

	//skip initial msg
	for i := 0; i < length; i++ {
		for j := 0; j < length-i; j++ {
			_, _, _ = conns[i].ReadMessage()
		}
	}

	defer teardown1()
	defer teardown2()
	defer teardown3()
	defer teardown4()
	var readCounter int32
	var writeCounter int32

	msg, _ := jsoniter.Marshal(
		moveRequest{
			Action: ch.MoveToAction,
			Move:   ch.Position{X: ch.Point{Int: 1, Dec: 0}},
		},
	)

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	wsReadLoop := func(ctx context.Context, conn *websocket.Conn) {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				_, _, err := conn.ReadMessage()
				if err != nil {
					return
				}
				atomic.AddInt32(&readCounter, 1)
			}
		}
	}

	for _, conn := range conns[1:] {
		go wsReadLoop(ctx, conn)
	}

	start := time.Now()
	for i := 0; i < 5_000_000; i++ {
		_ = conn1.WriteMessage(websocket.TextMessage, msg)
		atomic.AddInt32(&writeCounter, 1)
	}

	result(time.Since(start), readCounter, writeCounter, int32(len(conns)-1))

	runtime.ReadMemStats(&rtm)
	printMemStats(os.Stdout, "result", rtm)
}

func result(el time.Duration, read, write, countReaders int32) {
	rpsAbs := int32(float64(read) / el.Seconds())
	rps := int32(float64(read/countReaders) / el.Seconds())
	loses := float32(write-read/countReaders) / float32(write) * 100

	log.Printf(
		"\nbench finished\nelapsed time: %v\nreadMessages: %d\nwriteMessages: %d\nrps abs: %d\nrps: %d\nloses: %.2f%%\n",
		el, read/countReaders, write, rpsAbs, rps, loses,
	)
}

func printMemStats(wr io.Writer, msg string, rtm runtime.MemStats) {
	_, _ = fmt.Fprintf(wr, "===%s===\n", msg)
	_, _ = fmt.Fprintln(wr, "Mallocs: ", rtm.Mallocs)
	_, _ = fmt.Fprintln(wr, "Frees: ", rtm.Frees)
	_, _ = fmt.Fprintln(wr, "LiveObjects: ", rtm.Mallocs-rtm.Frees)
	_, _ = fmt.Fprintln(wr, "PauseTotalNs: ", rtm.PauseTotalNs)
	_, _ = fmt.Fprintln(wr, "NumGC: ", rtm.NumGC)
	_, _ = fmt.Fprintln(wr, "LastGC: ", time.UnixMilli(int64(rtm.LastGC/1_000_000)))
	_, _ = fmt.Fprintln(wr, "HeapObjects: ", rtm.HeapObjects)
	_, _ = fmt.Fprintln(wr, "HeapAlloc: ", rtm.HeapAlloc)
}

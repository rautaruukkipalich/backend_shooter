package testServer

import (
	"backend_shooter/internal/api"
	"backend_shooter/internal/dto"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	jsoniter "github.com/json-iterator/go"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
)

type Client struct {
	url string
}

func NewClient() *Client {
	return &Client{
		url: cfg.Server.Addr,
	}
}

func (c *Client) Register(_ context.Context, form any) *httptest.ResponseRecorder {
	b, _ := json.Marshal(form)

	r, _ := http.NewRequest(
		http.MethodPost,
		"/auth/register",
		strings.NewReader(string(b)),
	)
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("accept", "application/json")
	w := httptest.NewRecorder()

	SRV.ServeHTTP(w, r)

	return w
}

func (c *Client) Login(_ context.Context, form any) *httptest.ResponseRecorder {
	b, _ := json.Marshal(form)

	r, _ := http.NewRequest(
		http.MethodPost,
		"/auth/login",
		strings.NewReader(string(b)),
	)
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("accept", "application/json")
	w := httptest.NewRecorder()

	SRV.ServeHTTP(w, r)
	return w
}

func (c *Client) CreateCharacter(_ context.Context, token string, form any) *httptest.ResponseRecorder {
	b, _ := json.Marshal(form)

	r, _ := http.NewRequest(
		http.MethodPost,
		"/character",
		strings.NewReader(string(b)),
	)
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("accept", "application/json")
	r.Header.Add(api.AuthorizationHeader, fmt.Sprintf("Bearer %s", token))

	w := httptest.NewRecorder()

	SRV.ServeHTTP(w, r)
	return w
}

func (c *Client) GetCharacters(_ context.Context, token string) *httptest.ResponseRecorder {
	r, _ := http.NewRequest(
		http.MethodGet,
		"/character",
		nil,
	)
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("accept", "application/json")
	r.Header.Add(api.AuthorizationHeader, fmt.Sprintf("Bearer %s", token))

	w := httptest.NewRecorder()

	SRV.ServeHTTP(w, r)
	return w
}

func (c *Client) ChooseCharacter(_ context.Context, token string, charID int) *httptest.ResponseRecorder {
	b, _ := json.Marshal(dto.IDRequest{ID: charID})

	r, _ := http.NewRequest(
		http.MethodPost,
		"/character/choose",
		strings.NewReader(string(b)),
	)
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("accept", "application/json")
	r.Header.Add(api.AuthorizationHeader, fmt.Sprintf("Bearer %s", token))

	w := httptest.NewRecorder()

	SRV.ServeHTTP(w, r)
	return w
}

func (c *Client) ConnectWS(
	ctx context.Context,
	userForm any,
	charForm any,
) (*websocket.Conn, func()) {
	////register user
	//_ = c.Register(ctx, userForm)
	//login user
	w := c.Login(ctx, userForm)
	var token dto.TokenResponse
	if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
		log.Fatal(err)
	}

	//create char
	_ = c.CreateCharacter(ctx, token.Token, charForm)
	//get list chars
	w = c.GetCharacters(ctx, token.Token)
	var chars dto.CharactersResponse
	if err := jsoniter.NewDecoder(w.Body).Decode(&chars); err != nil {
		log.Fatal(err)
	}
	//choose char
	char := int(chars.Characters[0].ID)
	w = c.ChooseCharacter(ctx, token.Token, char)
	if err := jsoniter.NewDecoder(w.Body).Decode(&token); err != nil {
		log.Fatal(err)
	}

	url := fmt.Sprintf("ws://%s/ws", c.url)
	header := http.Header{
		"Authorization": []string{fmt.Sprintf("Bearer %s", token.Token)},
	}

	// Connect to the server
	ws, _, err := websocket.DefaultDialer.Dial(url, header)
	if err != nil {
		log.Fatalf("create ws: %v", err)
	}

	teardown := func() {
		if err := ws.Close(); err != nil {
			log.Fatalf("teardown ws: %v", err)
		}
	}

	return ws, teardown
}

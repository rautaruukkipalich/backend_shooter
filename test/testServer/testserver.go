package testServer

import (
	"backend_shooter/config"
	"backend_shooter/internal/api/auth"
	"backend_shooter/internal/api/character"
	"backend_shooter/internal/api/ws"
	"backend_shooter/internal/api/ws_pull"
	"backend_shooter/internal/api/ws_push"
	"backend_shooter/internal/database/arena_pull_slice"
	"backend_shooter/internal/database/arena_push_updates"
	"backend_shooter/internal/database/arenadb"
	"backend_shooter/internal/database/sqldb"
	mw "backend_shooter/internal/middleware"
	authService "backend_shooter/internal/services/auth"
	charService "backend_shooter/internal/services/character"
	charServicePull "backend_shooter/internal/services/character_pull_repo"
	charServicePush "backend_shooter/internal/services/character_push_repo"
	"backend_shooter/pkg/logger"
	"context"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

var cfg = &config.Config{
	Logger: config.LoggerConfig{
		Level: "debug",
		Type:  "text",
		Out:   "terminal",
	},
	Server: config.ServerConfig{
		Addr: "127.0.0.1:28080",
	},
	SQLDB: config.SQLDBConfig{
		URI:            "postgres://postgres:postgres@localhost:15433/shooter_test?sslmode=disable",
		MaxConnections: 5,
		Driver:         "postgres",
	},
}

type TestingServer struct {
	srv http.Server
	r   *mux.Router
}

func New(cfg *config.ServerConfig, r *mux.Router) *TestingServer {
	return &TestingServer{
		srv: http.Server{
			Addr:    cfg.Addr,
			Handler: r,
		},
		r: r,
	}
}

func (s *TestingServer) MustRun() {
	if err := s.srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func (s *TestingServer) Stop() {
	_ = s.srv.Shutdown(context.Background())
}

func (s *TestingServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.r.ServeHTTP(w, r)
}

var SRV = OldTestServer()

func OldTestServer() *TestingServer {
	l, err := logger.ForTests()
	if err != nil {
		log.Fatal(err)
	}

	fastStore, err := arenadb.New()
	if err != nil {
		log.Fatal(err)
	}

	sqlStore, err := sqldb.New(&cfg.SQLDB)
	if err != nil {
		log.Fatal(err)
	}

	authSrv := authService.New(sqlStore)
	charSrv := charService.New(sqlStore, fastStore)

	r := mux.NewRouter()

	mw.AddLogger(l)
	r.Use(mw.Logger)
	r.Use(mw.JWT)

	authRouter := r.PathPrefix("/auth").Subrouter()
	auth.RegisterRouter(authSrv, authRouter)

	charRouter := r.PathPrefix("/character").Subrouter()
	character.RegisterRouter(charSrv, charRouter)
	charRouter.Use(mw.Auth)

	wsRouter := r.PathPrefix("/ws").Subrouter()
	ws.RegisterRouter(charSrv, wsRouter)
	wsRouter.Use(mw.Auth)
	wsRouter.Use(mw.WSMetrics)

	l.Info("starting server")

	srv := New(&cfg.Server, r)
	return srv
}

var SRVpush = NewPushServer()

func NewPushServer() *TestingServer {
	l, err := logger.ForTests()
	if err != nil {
		log.Fatal(err)
	}

	fastStore, err := arena_push_updates.New()
	if err != nil {
		log.Fatal(err)
	}

	sqlStore, err := sqldb.New(&cfg.SQLDB)
	if err != nil {
		log.Fatal(err)
	}

	authSrv := authService.New(sqlStore)
	charSrv := charServicePush.New(sqlStore, fastStore)

	r := mux.NewRouter()

	mw.AddLogger(l)
	r.Use(mw.Logger)
	r.Use(mw.JWT)

	authRouter := r.PathPrefix("/auth").Subrouter()
	auth.RegisterRouter(authSrv, authRouter)

	charRouter := r.PathPrefix("/character").Subrouter()
	character.RegisterRouter(charSrv, charRouter)
	charRouter.Use(mw.Auth)

	wsRouter := r.PathPrefix("/ws").Subrouter()
	ws_push.RegisterRouter(charSrv, wsRouter)
	wsRouter.Use(mw.Auth)
	wsRouter.Use(mw.WSMetrics)

	l.Info("starting server")

	srv := New(&cfg.Server, r)
	return srv
}

var SRVpull = NewPullServer()

func NewPullServer() *TestingServer {
	l, err := logger.ForTests()
	if err != nil {
		log.Fatal(err)
	}

	fastStore, err := arena_pull_slice.New()
	if err != nil {
		log.Fatal(err)
	}

	sqlStore, err := sqldb.New(&cfg.SQLDB)
	if err != nil {
		log.Fatal(err)
	}

	authSrv := authService.New(sqlStore)
	charSrv := charServicePull.New(sqlStore, fastStore)

	r := mux.NewRouter()

	mw.AddLogger(l)
	r.Use(mw.Logger)
	r.Use(mw.JWT)

	authRouter := r.PathPrefix("/auth").Subrouter()
	auth.RegisterRouter(authSrv, authRouter)

	charRouter := r.PathPrefix("/character").Subrouter()
	character.RegisterRouter(charSrv, charRouter)
	charRouter.Use(mw.Auth)

	wsRouter := r.PathPrefix("/ws").Subrouter()
	ws_pull.RegisterRouter(charSrv, wsRouter)
	wsRouter.Use(mw.Auth)
	wsRouter.Use(mw.WSMetrics)

	l.Info("starting server")

	srv := New(&cfg.Server, r)
	return srv
}

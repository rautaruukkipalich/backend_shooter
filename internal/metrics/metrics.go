package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"strconv"
	"time"
)

func Listen(addr string) error {
	mux := http.NewServeMux()

	mux.Handle("/metrics", promhttp.Handler())

	return http.ListenAndServe(addr, mux)
}

var (
	ActiveWSConnections = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "ws_connections_active_count",
		Help: "Number of active websocket connections",
	})

	TotalWSConnections = promauto.NewCounter(prometheus.CounterOpts{
		Name: "ws_connections_total",
		Help: "Number of total websocket connections",
	})

	TotalWSMessages = promauto.NewCounter(prometheus.CounterOpts{
		Name: "ws_messages_total",
		Help: "Number of total websocket messages",
	})

	TotalPostRequests = promauto.NewCounter(prometheus.CounterOpts{
		Name: "http_post_requests_total",
		Help: "Number of post requests",
	})

	TotalGetRequests = promauto.NewCounter(prometheus.CounterOpts{
		Name: "http_get_requests_total",
		Help: "Number of get requests",
	})
)

var histogramCodeVec = promauto.NewHistogramVec(
	prometheus.HistogramOpts{
		Namespace: "hist",
		Name:      "response_time",
		Help:      "Time handled by status code",
	},
	[]string{
		"code",
	},
)

func ObserveHistogramCodeResponseVec(code int, dur time.Duration) {
	histogramCodeVec.WithLabelValues(
		strconv.Itoa(code),
	).Observe(dur.Seconds())
}

var histogramMethodCounter = promauto.NewSummaryVec(
	prometheus.SummaryOpts{
		Namespace: "hist",
		Name:      "method",
		Help:      "Request method",
	},
	[]string{
		"method",
		"path",
	},
)

func ObserveHistogramMethodResponseVec(method string, path string, dur time.Duration) {
	histogramMethodCounter.WithLabelValues(
		method,
		path,
	).Observe(dur.Seconds())
}

var histogramTimeQueryCounter = promauto.NewHistogramVec(
	prometheus.HistogramOpts{
		Namespace: "hist",
		Name:      "query_time",
		Help:      "sql query time",
	},
	[]string{
		"query",
	},
)

func ObserveHistogramTimeQueryCounter(query string, dur time.Duration) {
	histogramTimeQueryCounter.WithLabelValues(
		query,
	).Observe(dur.Seconds())
}

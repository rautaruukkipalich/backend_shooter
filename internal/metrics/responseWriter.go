package metrics

import "net/http"

type CustomResponseWriter struct {
	rw         http.ResponseWriter
	StatusCode int
}

func ExtendResponseWriter(w http.ResponseWriter) *CustomResponseWriter {
	return &CustomResponseWriter{w, http.StatusOK}
}

func (w *CustomResponseWriter) WriteHeader(statusCode int) {
	w.StatusCode = statusCode
	w.rw.WriteHeader(statusCode)
}

func (w *CustomResponseWriter) Write(b []byte) (int, error) {
	return w.rw.Write(b)
}

func (w *CustomResponseWriter) Header() http.Header {
	return w.rw.Header()
}

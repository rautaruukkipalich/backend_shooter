package server

import (
	"backend_shooter/config"
	"context"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type Server struct {
	srv http.Server
	r   *mux.Router
}

func New(cfg *config.ServerConfig, r *mux.Router) *Server {
	return &Server{
		srv: http.Server{
			Addr:    cfg.Addr,
			Handler: r,
		},
		r: r,
	}
}

func (s *Server) MustRun() {
	if err := s.srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}

func (s *Server) Stop() {
	_ = s.srv.Shutdown(context.Background())
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.r.ServeHTTP(w, r)
}

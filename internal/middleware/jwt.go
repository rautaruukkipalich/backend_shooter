package middleware

import (
	"backend_shooter/internal/api"
	"backend_shooter/pkg/jwt"
	"context"
	"net/http"
)

func JWT(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), api.JWTKey, jwt.New())
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

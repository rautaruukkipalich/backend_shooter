package middleware

import (
	"backend_shooter/internal/metrics"
	"net/http"
	"time"
)

func Metrics(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			switch r.Method {
			case http.MethodGet:
				metrics.TotalGetRequests.Inc()
			case http.MethodPost:
				metrics.TotalPostRequests.Inc()
			}
			start := time.Now()
			ResponseWriter := metrics.ExtendResponseWriter(w)

			next.ServeHTTP(ResponseWriter, r)

			metrics.ObserveHistogramCodeResponseVec(ResponseWriter.StatusCode, time.Since(start))
			metrics.ObserveHistogramMethodResponseVec(r.Method, r.RequestURI, time.Since(start))
		},
	)
}

func WSMetrics(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r)
		},
	)
}

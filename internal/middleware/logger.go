package middleware

import (
	"backend_shooter/pkg/logger"
	"log/slog"
	"net/http"
)

var (
	log *slog.Logger
)

func AddLogger(l *slog.Logger) {
	log = l
}

func Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			ctx := logger.AddRequestInfo(r.Context(), r)
			next.ServeHTTP(w, r.WithContext(logger.ToContext(ctx, log)))
		},
	)
}

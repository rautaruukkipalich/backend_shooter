package cors

import (
	"backend_shooter/config"
	"github.com/rs/cors"
)

func New(cfg *config.CorsConfig) *cors.Cors {
	return cors.New(
		cors.Options{
			AllowedOrigins:   cfg.AllowedOrigins,
			AllowedMethods:   cfg.AllowedMethods,
			AllowedHeaders:   cfg.AllowedHeaders,
			AllowCredentials: cfg.AllowCredentials,
		},
	)
}

package middleware

import (
	"backend_shooter/internal/api"
	"backend_shooter/internal/dto"
	"context"
	"net/http"
	"strings"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()
			authHeader := r.Header.Get(api.AuthorizationHeader)
			authHeaderChunks := strings.Split(authHeader, " ")
			if len(authHeaderChunks) != 2 {
				api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusUnauthorized, Error: ErrInvalidToken.Error()})
				return
			}

			jwtPayload, err := api.JWTEncodeDecoder.Decode(ctx, authHeaderChunks[1])
			if err != nil {
				api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusUnauthorized, Error: ErrInvalidToken.Error()})
				return
			}

			ctx = context.WithValue(ctx, api.UserIDKey, jwtPayload.UserID)
			ctx = context.WithValue(ctx, api.CharIDKey, jwtPayload.CharID)
			next.ServeHTTP(w, r.WithContext(ctx))
		},
	)
}

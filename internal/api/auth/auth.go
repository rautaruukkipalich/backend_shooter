package auth

import (
	"backend_shooter/internal/api"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model/user"
	"context"
	"github.com/gorilla/mux"
	"net/http"
)

type Auth interface {
	Register(context.Context, dto.LoginRequest, user.PasswordHasher) error
	Login(context.Context, dto.LoginRequest, user.PasswordHasher) (int64, error)
}

type Router struct {
	auth   Auth
	router *mux.Router
}

func RegisterRouter(auth Auth, r *mux.Router) *Router {
	rt := &Router{auth: auth, router: r}
	rt.router.HandleFunc("/register", rt.register).Methods(http.MethodPost)
	rt.router.HandleFunc("/login", rt.login).Methods(http.MethodPost)
	rt.router.HandleFunc("/logout", rt.logout).Methods(http.MethodPost)
	return rt
}

// @Summary		Register
// @Description	register by username and password
// @Tags			auth
// @Accept			json
// @Produce		json
// @Param			input	body		dto.LoginRequest	true	"register"
// @Success		200,201	{object}	nil
// @Failure		400,404	{object}	dto.ErrorResponse
// @Success		500		{object}	dto.ErrorResponse
// @Success		default	{object}	dto.ErrorResponse
// @Router			/auth/register [post]
func (rt *Router) register(w http.ResponseWriter, r *http.Request) {
	var request dto.LoginRequest
	ctx := r.Context()

	if err := api.GetFromBody(r, &request); err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	if err := rt.auth.Register(ctx, request, user.Hasher); err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}
	api.RespondJSON(ctx, w, http.StatusCreated, nil)
}

// @Summary		Login
// @Description	get token by username and password
// @Tags			auth
// @Accept			json
// @Produce		json
// @Param			input		body		dto.LoginRequest	true	"login"
// @Success		200			{object}	dto.TokenResponse
// @Failure		400,401,404	{object}	dto.ErrorResponse
// @Success		500			{object}	dto.ErrorResponse
// @Success		default		{object}	dto.ErrorResponse
// @Router			/auth/login [post]
func (rt *Router) login(w http.ResponseWriter, r *http.Request) {
	var request dto.LoginRequest
	ctx := r.Context()

	if err := api.GetFromBody(r, &request); err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	uid, err := rt.auth.Login(ctx, request, user.Hasher)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	token, err := api.JWTEncodeDecoder.Encode(ctx, uid, 0)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	http.SetCookie(w, api.PrepareAuthCookie(ctx, token))

	api.RespondJSON(ctx, w, http.StatusOK, dto.TokenResponse{Token: token})
}

// @Summary		Logout
// @Security		AuthApiKey
// @Description	delete cookie token
// @Tags			auth
// @Accept			json
// @Produce		json
// @Success		200		{object}	nil
// @Failure		400,404	{object}	dto.ErrorResponse
// @Success		500		{object}	dto.ErrorResponse
// @Success		default	{object}	dto.ErrorResponse
// @Router			/auth/logout [post]
func (rt *Router) logout(w http.ResponseWriter, r *http.Request) {
	api.RespondJSON(r.Context(), w, 200, nil)
}

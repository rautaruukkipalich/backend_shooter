package metrics

import (
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type Router struct {
	router *mux.Router
}

func RegisterRouter(r *mux.Router) *Router {
	rt := &Router{router: r}
	rt.router.Handle("", promhttp.Handler())
	return rt
}

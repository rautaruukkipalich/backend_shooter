package api

import (
	"backend_shooter/internal/dto"
	"backend_shooter/pkg/logger"
	"context"
	"encoding/json"
	"log/slog"
	"net/http"
)

func RespondErr(ctx context.Context, w http.ResponseWriter, err dto.ErrorResponse) {
	RespondJSON(ctx, w, err.Code, err)
}

func RespondJSON(ctx context.Context, w http.ResponseWriter, code int, payload any) {
	w.WriteHeader(code)
	if payload != nil {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		if err := json.NewEncoder(w).Encode(payload); err != nil {
			logger.FromContext(ctx).Error("error writing response", slog.Any("error", err))
		}
	}
}

package api

import (
	"backend_shooter/pkg/jwt"
	"context"
)

var (
	JWTKey ctxKey = "JWT"

	JWTEncodeDecoder EncodeDecoder = &jwtEncodeDecoder{}
)

type EncodeDecoder interface {
	Encode(context.Context, int64, int64) (string, error)
	Decode(context.Context, string) (*jwt.DecodedJWT, error)
	GetTTL(context.Context) int
}

type jwtEncodeDecoder struct{}

func (j jwtEncodeDecoder) Encode(ctx context.Context, userID, charID int64) (string, error) {
	tokenizer, ok := ctx.Value(JWTKey).(*jwt.JWT)
	if !ok {
		return "", ErrInternalServerError
	}
	token, err := tokenizer.Encode(userID, charID)
	if err != nil {
		return "", ErrInternalServerError
	}
	return token, nil
}

func (j jwtEncodeDecoder) Decode(ctx context.Context, token string) (*jwt.DecodedJWT, error) {
	tokenizer, ok := ctx.Value(JWTKey).(*jwt.JWT)
	if !ok {
		return nil, ErrInternalServerError
	}
	decoded, err := tokenizer.Decode(token)
	if err != nil {
		return nil, ErrInternalServerError
	}
	return &decoded, nil
}

func (j jwtEncodeDecoder) GetTTL(ctx context.Context) int {
	return ctx.Value(JWTKey).(*jwt.JWT).GetTTL()
}

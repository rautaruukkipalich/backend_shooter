package ws_push

import (
	"backend_shooter/internal/api"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/metrics"
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/pkg/logger"
	"context"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	jsoniter "github.com/json-iterator/go"
	"log/slog"
	"net/http"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type FastChar interface {
	Connect(context.Context, *character.Character, chan *model.SliceStep) *character.Character
	Disconnect(context.Context, *character.Character)
	ArenaGetAll(context.Context, *character.Character) []character.Character
	Update(context.Context, *character.Character, []byte)
}

type SQLChar interface {
	GetByID(context.Context, int64, *character.Character) error
	CompareCharAndUser(context.Context, int64, int64) (bool, error)
	Save(context.Context, character.Character)
}

type Char interface {
	SQLChar
	FastChar
}

type Router struct {
	char   Char
	router *mux.Router
}

func RegisterRouter(char Char, r *mux.Router) *Router {
	rt := &Router{router: r, char: char}
	rt.router.HandleFunc("", rt.connectWS).Methods(http.MethodGet)
	return rt
}

// @Summary		Connect WS2
// @Security		AuthApiKey
// @Description	Shooter connect web socket
// @Tags			shooter_back
// @Success		101				{object}	nil
// @Failure		400,401,404,422	{object}	dto.ErrorResponse
// @Success		500				{object}	dto.ErrorResponse
// @Success		default			{object}	dto.ErrorResponse
// @Router			/ws2 [get]
func (rt *Router) connectWS(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := logger.FromContext(ctx)
	defer func() {
		if err := recover(); err != nil {
			log.Warn("recovered from panic", slog.Any("err", err))
		}
	}()

	charID, err := api.GetCharIDFromCtx(ctx)
	if err != nil {
		log.Error("error get char id from context", slog.Any("error", err))
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: api.ErrInvalidCharacterID.Error()})
		return
	}

	var char character.Character
	if err := rt.char.GetByID(ctx, charID, &char); err != nil {
		log.Error("error get char from db", slog.Any("error", err))
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	userID, err := api.GetUserIDFromCtx(ctx)
	if err != nil {
		log.Error("error get user id from context", slog.Any("error", err))
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: api.ErrNotAuthorized.Error()})
		return
	}

	if char.UserID != userID {
		log.Error("error char not from that user")
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: api.ErrInvalidCharacterID.Error()})
		return
	}

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	updCh := make(chan *model.SliceStep)
	defer close(updCh)

	arenaChar := rt.char.Connect(ctx, &char, updCh)
	if arenaChar == nil {
		log.Error("error connect to char", slog.Any("error", "out of limit"))
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusInternalServerError, Error: ErrOutOfLimit.Error()})
		return
	}

	//Установка таймаута для чтения сообщения
	//if err := conn.SetReadDeadline(time.Now().Add(a.wstimeout)); err != nil {
	//	log.Fatal(err)
	//}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Error("error create ws connection", slog.Any("err", err))
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: "use 'upgrade' header"})
		return
	}
	metrics.ActiveWSConnections.Inc()
	metrics.TotalWSConnections.Inc()

	defer func() {
		metrics.ActiveWSConnections.Dec()
		if err := conn.Close(); err != nil {
			logger.FromContext(ctx).Info("error conn close", slog.Any("err", err.Error()))
		}
	}()

	defer func() {
		rt.char.Disconnect(ctx, arenaChar)
		log.Info("disconnected")
	}()

	rt.handleWS(ctx, conn, arenaChar, updCh)
}

func (rt *Router) handleWS(ctx context.Context, conn *websocket.Conn, char *character.Character, updCh chan *model.SliceStep) {
	writeChan := make(chan []byte)
	requestChan := make(chan dto.CharacterRequest)
	readChan := make(chan struct{})

	defer func() {
		close(requestChan)
		close(writeChan)
	}()

	go rt.writeLoop(ctx, conn, writeChan)
	go rt.getUpdateLoop(ctx, updCh, char, writeChan)
	go rt.setUpdateLoop(ctx, char, requestChan)
	go rt.readLoop(ctx, conn, readChan, requestChan)
	go rt.saveToDBLoop(ctx, char)

	rt.initialResponse(ctx, char, writeChan)

	select {
	case <-ctx.Done():
		return
	case <-readChan:
		return
	}
}

func (rt *Router) initialResponse(ctx context.Context, char *character.Character, writeChan chan<- []byte) {
	log := logger.FromContext(ctx)
	chars := rt.char.ArenaGetAll(ctx, char)

	//enemies := make([]character.Character, 0, 100)
	//for _, en := range chars {
	//	enemies = append(enemies, *en)
	//	if char.CheckShowDistanceToEnemy(en) {
	//		enemies = append(enemies, *en)
	//	}
	//}

	resp := dto.WSCharactersResponse{
		Character: *char,
		Enemies:   chars,
	}
	msg, err := jsoniter.Marshal(resp)

	if err != nil {
		log.Info("error marshalling", slog.Any("err", err))
	}

	select {
	case <-ctx.Done():
		return
	case writeChan <- msg:
	}
}

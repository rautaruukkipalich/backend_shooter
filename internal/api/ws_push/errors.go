package ws_push

import "errors"

var (
	ErrOutOfLimit = errors.New("out of limit. reconnect later")
)

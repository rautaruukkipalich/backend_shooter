package character

import (
	"backend_shooter/internal/api"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model/character"
	"context"
	"github.com/gorilla/mux"
	"net/http"
)

type Char interface {
	Create(context.Context, int64, dto.CreateCharacterRequest) error
	GetByID(context.Context, int64, *character.Character) error
	GetCharactersByUserID(context.Context, int64) (dto.CharactersResponse, error)
	CompareCharAndUser(context.Context, int64, int64) (bool, error)
}

type Router struct {
	char   Char
	router *mux.Router
}

func RegisterRouter(char Char, r *mux.Router) *Router {
	rt := &Router{router: r, char: char}
	rt.router.HandleFunc("", rt.createCharacter).Methods(http.MethodPost)
	rt.router.HandleFunc("", rt.getCharacters).Methods(http.MethodGet)
	rt.router.HandleFunc("/{id:[0-9]+}", rt.getCharacter).Methods(http.MethodGet)
	rt.router.HandleFunc("/choose", rt.chooseCharacter).Methods(http.MethodPost)
	return rt
}

// @Summary		Create Character
// @Security		AuthApiKey
// @Description	create character
// @Tags			character
// @Accept			json
// @Produce		json
// @Param			input	body		dto.CreateCharacterRequest	true	"name"
// @Success		201		{object}	nil
// @Failure		400,404	{object}	dto.ErrorResponse
// @Success		500		{object}	dto.ErrorResponse
// @Success		default	{object}	dto.ErrorResponse
// @Router			/character [post]
func (rt *Router) createCharacter(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userID, err := api.GetUserIDFromCtx(ctx)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusUnauthorized, Error: err.Error()})
		return
	}

	var request dto.CreateCharacterRequest
	if err := api.GetFromBody(r, &request); err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	if err := rt.char.Create(ctx, userID, request); err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	api.RespondJSON(ctx, w, http.StatusCreated, nil)
}

// @Summary		Get Character
// @Security		AuthApiKey
// @Description	get character
// @Tags			character
// @Accept			json
// @Produce		json
// @Param			id		path		int	true	"character identifier"
// @Success		200		{object}	character.Character
// @Failure		400,404	{object}	dto.ErrorResponse
// @Success		500		{object}	dto.ErrorResponse
// @Success		default	{object}	dto.ErrorResponse
// @Router			/character/{id} [get]
func (rt *Router) getCharacter(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	_, err := api.GetUserIDFromCtx(ctx)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusUnauthorized, Error: err.Error()})
		return
	}

	charID, err := api.GetIdVarFromRequest(r)
	if err != nil || charID == api.ZeroValue {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: api.ErrInvalidCharacterID.Error()})
		return
	}

	var char character.Character
	if err := rt.char.GetByID(ctx, int64(charID), &char); err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	api.RespondJSON(r.Context(), w, 200, char)
}

// @Summary		Get Character list
// @Security		AuthApiKey
// @Description	get characters
// @Tags			character
// @Accept			json
// @Produce		json
// @Success		200		{object}	dto.CharactersResponse
// @Failure		400,404	{object}	dto.ErrorResponse
// @Success		500		{object}	dto.ErrorResponse
// @Success		default	{object}	dto.ErrorResponse
// @Router			/character [get]
func (rt *Router) getCharacters(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userID, err := api.GetUserIDFromCtx(ctx)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusUnauthorized, Error: err.Error()})
		return
	}

	chars, err := rt.char.GetCharactersByUserID(ctx, userID)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: err.Error()})
		return
	}

	api.RespondJSON(r.Context(), w, 200, chars)
}

// @Summary		Choose Character
// @Security		AuthApiKey
// @Description	choose character
// @Tags			character
// @Accept			json
// @Produce		json
// @Param			input	body		dto.IDRequest	true	"id"
// @Success		200		{object}	dto.TokenResponse
// @Failure		400,404	{object}	dto.ErrorResponse
// @Success		500		{object}	dto.ErrorResponse
// @Success		default	{object}	dto.ErrorResponse
// @Router			/character/choose [post]
func (rt *Router) chooseCharacter(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	userID, err := api.GetUserIDFromCtx(ctx)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusUnauthorized, Error: err.Error()})
		return
	}

	var request dto.IDRequest
	if err := api.GetFromBody(r, &request); err != nil || request.ID == api.ZeroValue {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: api.ErrInvalidCharacterID.Error()})
		return
	}

	charID := int64(request.ID)

	ok, err := rt.char.CompareCharAndUser(ctx, userID, charID)
	if err != nil || !ok {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusBadRequest, Error: api.ErrInvalidCharacterID.Error()})
		return
	}

	token, err := api.JWTEncodeDecoder.Encode(ctx, userID, charID)
	if err != nil {
		api.RespondErr(ctx, w, dto.ErrorResponse{Code: http.StatusInternalServerError, Error: err.Error()})
		return
	}

	http.SetCookie(w, api.PrepareAuthCookie(ctx, token))

	api.RespondJSON(r.Context(), w, 200, dto.TokenResponse{Token: token})
}

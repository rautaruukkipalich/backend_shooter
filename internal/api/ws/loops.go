package ws

import (
	"backend_shooter/internal/dto"
	"backend_shooter/internal/metrics"
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/pkg/logger"
	"context"
	"github.com/gorilla/websocket"
	jsoniter "github.com/json-iterator/go"
	"log/slog"
	"time"
)

const (
	//readUpdatesPeriod = 10 * time.Millisecond
	savePeriod = 10 * time.Second
)

func (rt *Router) readLoop(
	ctx context.Context,
	conn *websocket.Conn,
	readChan chan struct{},
	reqChan chan<- dto.CharacterRequest,
) {

	defer close(readChan)
	log := logger.FromContext(ctx)

	defer func() {
		if r := recover(); r != nil {
			log.Error("readLoop recovered from panic", slog.Any("err", r))
		}
	}()

	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			if !websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Debug("error read from ws", slog.Any("err", err))
			}
			return
		}
		metrics.TotalWSMessages.Inc()
		if len(msg) == 0 {
			continue
		}

		var request dto.CharacterRequest
		if err := jsoniter.Unmarshal(msg, &request); err != nil {
			log.Debug("error unmarshall msg", slog.Any("err", err))
			continue
		}

		select {
		case <-ctx.Done():
			return
		default:
			reqChan <- request
		}
	}
}

func (rt *Router) setUpdateLoop(
	ctx context.Context,
	char *character.Character,
	reqChan <-chan dto.CharacterRequest,
) {

	log := logger.FromContext(ctx)

	defer func() {
		if r := recover(); r != nil {
			log.Error("setUpdateLoop recovered from panic", slog.Any("err", r))
		}
	}()

	for req := range reqChan {
		var respDTO = dto.WSCharacterActionResponse{
			Char: *char,
		}
		switch req.Action {
		case character.MoveAction:
			char.Move(&req.Move)
			respDTO.Action = character.MoveAction
			respDTO.Position = req.Move

		case character.MoveToAction:
			char.MoveTo(&req.Move)
			respDTO.Action = character.MoveToAction
			respDTO.Position = req.Move

		default:
			log.Debug("unknown action", slog.Any("action", req.Action))
			continue
		}

		resp, err := jsoniter.Marshal(respDTO)
		if err != nil {
			logger.FromContext(ctx).Info("error marshalling", slog.Any("err", err))
			return
		}

		select {
		case <-ctx.Done():
			return
		default:
			rt.char.Update(ctx, char, resp)
		}
	}

}

func (rt *Router) getUpdateLoop(
	ctx context.Context,
	head *model.Steps,
	char *character.Character,
	writeChan chan []byte,
) {
	log := logger.FromContext(ctx)

	defer func() {
		if r := recover(); r != nil {
			log.Error("getUpdateLoop recovered from panic", slog.Any("err", r))
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		default:
			//rt.char.Wait()
			if head.Next == nil {
				continue
			}
			head = head.Next
			if head.CharID == char.ID {
				continue
			}
			select {
			case <-ctx.Done():
			case writeChan <- head.Step:
			}
		}
	}
}

func (rt *Router) writeLoop(ctx context.Context, conn *websocket.Conn, dataCh chan []byte) {
	log := logger.FromContext(ctx)

	defer func() {
		if r := recover(); r != nil {
			log.Error("writeLoop recovered from panic", slog.Any("err", r))
		}
	}()

	for data := range dataCh {
		if err := conn.WriteMessage(websocket.TextMessage, data); err != nil {
			log.Debug("error write msg", slog.Any("err", err))
			return
		}
	}
}

func (rt *Router) saveToDBLoop(ctx context.Context, ch *character.Character) {
	ticker := time.NewTicker(savePeriod)

	defer func() {
		go rt.char.Save(context.Background(), *ch)
		defer ticker.Stop()
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			go rt.char.Save(ctx, *ch)
		}
	}
}

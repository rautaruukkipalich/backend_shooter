package ws_pull

import "errors"

var (
	ErrOutOfLimit = errors.New("out of limit. reconnect later")
)

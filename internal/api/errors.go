package api

import "errors"

var (
	ErrNotAuthorized       = errors.New("not authorized")
	ErrInvalidToken        = errors.New("invalid token")
	ErrEmptyCharacterName  = errors.New("empty character name")
	ErrInvalidCharacterID  = errors.New("invalid character id")
	ErrInternalServerError = errors.New("internal server error")
)

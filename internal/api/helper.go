package api

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	jsoniter "github.com/json-iterator/go"
	"net/http"
	"strconv"
)

type ctxKey string

var (
	UserIDKey ctxKey = "UserID"
	CharIDKey ctxKey = "CharID"
)

func GetFromBody(r *http.Request, form any) error {
	defer func() {
		_ = r.Body.Close()
	}()
	return jsoniter.NewDecoder(r.Body).Decode(form)
}

func GetIdVarFromRequest(r *http.Request) (int, error) {
	vars := mux.Vars(r)
	strID := vars["id"]
	return strconv.Atoi(strID)
}

func GetUserIDFromCtx(ctx context.Context) (int64, error) {
	userID, ok := ctx.Value(UserIDKey).(int64)
	if !ok || userID == ZeroValue {
		return 0, ErrNotAuthorized
	}
	return userID, nil
}

func GetCharIDFromCtx(ctx context.Context) (int64, error) {
	charID, ok := ctx.Value(CharIDKey).(int64)
	if !ok || charID == ZeroValue {
		return 0, ErrNotAuthorized
	}
	return charID, nil
}

func PrepareAuthCookie(ctx context.Context, token string) *http.Cookie {
	return &http.Cookie{
		Name:     AuthorizationHeader,
		Value:    fmt.Sprintf("Bearer %s", token),
		Path:     "/",
		MaxAge:   JWTEncodeDecoder.GetTTL(ctx),
		HttpOnly: true,
	}
}

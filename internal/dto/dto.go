package dto

import (
	"backend_shooter/internal/model/character"
)

type (
	LoginRequest struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}

	TokenResponse struct {
		Token string `json:"token"`
	}

	CreateCharacterRequest struct {
		Name string `json:"name"`
	}

	CharacterRequest struct {
		Action    character.Action    `json:"action"`
		Character character.Character `json:"character,omitempty"`
		Move      character.Position  `json:"move,omitempty"`
	}

	CharacterResponse struct {
		character.Character
	}

	CharactersResponse struct {
		Characters []character.Character `json:"characters"`
	}

	ErrorResponse struct {
		Code  int    `json:"code"`
		Error string `json:"error"`
	}

	IDRequest struct {
		ID int `json:"id"`
	}

	WSCharactersResponse struct {
		Character character.Character   `json:"me"`
		Enemies   []character.Character `json:"enemies,omitempty"`
	}

	WSCharacterActionResponse struct {
		Char     character.Character `json:"char"`
		Action   character.Action    `json:"action"`
		Position character.Position  `json:"position,omitempty"`
	}
)

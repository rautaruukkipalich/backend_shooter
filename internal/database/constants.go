package database

const (
	CharByID       = "charId=%s"
	UserByID       = "userId=%s"
	CharByName     = "charName=%s"
	UserByUsername = "userName=%s"
)

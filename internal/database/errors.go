package database

import "errors"

var (
	ErrNoRows                = errors.New("err no rows")
	ErrConnectDB             = errors.New("error connecting to database")
	ErrUserNotFound          = errors.New("user not found")
	ErrUserAlreadyExist      = errors.New("user already exist")
	ErrCharacterNotFound     = errors.New("character not found")
	ErrCharacterAlreadyExist = errors.New("character already exist")
)

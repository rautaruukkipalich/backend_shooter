package arenadb

import (
	"testing"
)

func testArena(t *testing.T) (*ArenaStore, func()) {
	t.Helper()
	fastStore, err := New()
	if err != nil {
		t.Fatal(err)
	}
	teardown := func() {
		fastStore.store.Free()
	}
	return fastStore, teardown
}

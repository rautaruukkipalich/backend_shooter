package arenadb

import (
	"backend_shooter/internal/model/character"
	"context"
	"github.com/stretchr/testify/assert"
	"runtime"
	"testing"
	"unsafe"
)

func Test_Connect(t *testing.T) {
	db, teardown := testArena(t)
	defer teardown()
	ctx := context.Background()
	char := character.New("testChar", 1)

	type CharAssert struct {
		name    string
		memChar *character.Character
	}

	tcs := []CharAssert{
		{
			name:    "expect no error",
			memChar: char,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			arenaChar := db.Character().Connect(ctx, char)

			assert.NotNil(t, arenaChar)
			assert.Equal(t, char, arenaChar)
			assert.NotEqual(t, unsafe.Pointer(char), unsafe.Pointer(arenaChar))
			assert.Equal(t, char.Name, arenaChar.Name)
		})
	}
}

func Test_Disconnect(t *testing.T) {
	db, teardown := testArena(t)
	defer teardown()
	ctx := context.Background()
	char := character.New("testChar", 1)
	arenaChar := db.Character().Connect(ctx, char)

	type CharAssert struct {
		name      string
		memChar   *character.Character
		arenaChar *character.Character
	}

	tcs := []CharAssert{
		{
			name:      "expect no error",
			arenaChar: arenaChar,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			ptrArena := unsafe.Pointer(tc.arenaChar)
			ptrMem := unsafe.Pointer(tc.memChar)

			db.Character().Disconnect(ctx, arenaChar)

			runtime.GC()

			chArena := (*character.Character)(ptrArena)
			chMem := (*character.Character)(ptrMem)

			assert.NotNil(t, chArena)
			assert.Nil(t, chMem)
			assert.NotEqual(t, chArena, chMem)
		})
	}

}

func Test_GetHead(t *testing.T) {
	db, teardown := testArena(t)
	defer teardown()
	ctx := context.Background()
	head := db.Character().GetHead(ctx)
	assert.NotNil(t, head)
	assert.Nil(t, head.Next)

	char := character.New("testChar", 1)
	char.ID = 1
	db.Character().Connect(ctx, char)

	assert.NotNil(t, head.Next)
	head = head.Next

	step := head.Step

	assert.Nil(t, head.Next)
	assert.Equal(t, head.CharID, char.ID)

	db.Character().Disconnect(ctx, char)
	assert.NotNil(t, head.Next)
	head = head.Next

	step2 := head.Step

	assert.Nil(t, head.Next)
	assert.Equal(t, head.CharID, char.ID)

	assert.NotEqual(t, step, step2)
}

func Test_Update(t *testing.T) {
	db, teardown := testArena(t)
	defer teardown()
	ctx := context.Background()
	char := character.New("testChar", 1)
	char.ID = 1

	head := db.Character().GetHead(ctx)
	assert.NotNil(t, head)

	arenaChar := db.Character().Connect(ctx, char)
	head = head.Next

	data := []byte("test")
	db.Character().Update(ctx, arenaChar, data)
	head = head.Next
	assert.Equal(t, data, head.Step)
}

func Test_ArenaGetAll(t *testing.T) {
	db, teardown := testArena(t)
	defer teardown()
	ctx := context.Background()

	char := character.New("testChar", 1)
	char.ID = 1
	arenaChar := db.Character().Connect(ctx, char)
	assert.Equal(t, 0, len(db.Character().ArenaGetAll(ctx, arenaChar)))

	char2 := character.New("testChar2", 2)
	char2.ID = 2
	arenaChar2 := db.Character().Connect(ctx, char2)
	assert.Equal(t, 1, len(db.Character().ArenaGetAll(ctx, arenaChar2)))

	char3 := character.New("testChar3", 3)
	char3.ID = 3
	arenaChar3 := db.Character().Connect(ctx, char3)
	assert.Equal(t, 2, len(db.Character().ArenaGetAll(ctx, arenaChar3)))

	db.Character().Disconnect(ctx, arenaChar)
	assert.Equal(t, 1, len(db.Character().ArenaGetAll(ctx, arenaChar2)))
	assert.Equal(t, 1, len(db.Character().ArenaGetAll(ctx, arenaChar3)))

}

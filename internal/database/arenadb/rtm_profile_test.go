package arenadb

import (
	"backend_shooter/internal/model/character"
	"context"
	"fmt"
	"io"
	"os"
	"runtime"
	"testing"
	"time"
)

const (
	testIter = 1_000_000
)

func Test_RtmArena(t *testing.T) {
	if os.Getenv("SKIPTEST") == "" {
		t.Skip("skip test")
	}
	db, _ := New()
	defer db.Stop()

	ctx := context.Background()

	for i := 0; i < 1000; i++ {
		name := fmt.Sprintf("char%d", i)
		char := character.New(name, 1)
		char.ID = int64(i)
		_ = db.Character().Connect(ctx, char)
	}

	testChar := character.New("testChar", 1)

	f, stop := openFile("rtm_arena.txt")
	defer stop()

	var rtm runtime.MemStats

	for i := 0; i < testIter; i++ {
		db.Character().ArenaGetAll(ctx, testChar)
	}
	runtime.ReadMemStats(&rtm)
	printMemStats(f, fmt.Sprintf("rtm arena get all %d", testIter), rtm)
}

func Test_RtmNonArena(t *testing.T) {
	if os.Getenv("SKIPTEST") == "" {
		t.Skip("skip test")
	}
	db, _ := newNonArenaStore()
	defer db.Stop()

	ctx := context.Background()

	for i := 0; i < 1000; i++ {
		name := fmt.Sprintf("char%d", i)
		char := character.New(name, 1)
		char.ID = int64(i)
		_ = db.Character().Connect(ctx, char)
	}

	testChar := character.New("testChar", 1)

	f, stop := openFile("rtm_nonArena.txt")
	defer stop()

	var rtm runtime.MemStats

	for i := 0; i < testIter; i++ {
		db.Character().ArenaGetAll(ctx, testChar)
	}
	runtime.ReadMemStats(&rtm)
	printMemStats(f, fmt.Sprintf("rtm non arena get all %d", testIter), rtm)
}

func openFile(filename string) (*os.File, func()) {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}
	if err := os.Truncate(f.Name(), 0); err != nil {
		panic(err)
	}
	stop := func() {
		_ = f.Close()
	}

	return f, stop
}

func printMemStats(wr io.Writer, msg string, rtm runtime.MemStats) {
	_, _ = fmt.Fprintf(wr, "===%s===", msg)
	_, _ = fmt.Fprintln(wr, "Mallocs: ", rtm.Mallocs)
	_, _ = fmt.Fprintln(wr, "Frees: ", rtm.Frees)
	_, _ = fmt.Fprintln(wr, "LiveObjects: ", rtm.Mallocs-rtm.Frees)
	_, _ = fmt.Fprintln(wr, "PauseTotalNs: ", rtm.PauseTotalNs)
	_, _ = fmt.Fprintln(wr, "NumGC: ", rtm.NumGC)
	_, _ = fmt.Fprintln(wr, "LastGC: ", time.UnixMilli(int64(rtm.LastGC/1_000_000)))
	_, _ = fmt.Fprintln(wr, "HeapObjects: ", rtm.HeapObjects)
	_, _ = fmt.Fprintln(wr, "HeapAlloc: ", rtm.HeapAlloc)
}

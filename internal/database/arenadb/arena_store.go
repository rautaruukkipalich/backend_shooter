package arenadb

import (
	"arena"
	"backend_shooter/internal/database"
)

type ArenaStore struct {
	store      *arena.Arena
	characters database.CharacterFastRepo
}

func New() (*ArenaStore, error) {
	as := &ArenaStore{store: arena.NewArena()}

	characterRepo, err := NewCharacterRepo(as)
	if err != nil {
		return nil, err
	}

	as.characters = characterRepo

	return as, nil
}

func (as *ArenaStore) Character() database.CharacterFastRepo {
	return as.characters
}

func (as *ArenaStore) Stop() {
	as.store.Free()
}

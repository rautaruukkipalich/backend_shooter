package arenadb

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_Arena_Create(t *testing.T) {
	fastStore, err := New()
	if err != nil {
		t.Fatal(err)
	}
	assert.NotNil(t, fastStore)
}

package arenadb

import (
	"backend_shooter/internal/database"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/pkg/logger"
	"context"
	jsoniter "github.com/json-iterator/go"
	"sync"
	"sync/atomic"
)

type nonArenaStore struct {
	store      *nonArena
	characters database.CharacterFastRepo
}

func newNonArenaStore() (*nonArenaStore, error) {
	a := &nonArena{}
	as := &nonArenaStore{store: a}

	characterRepo, err := newNonArenaCharacterRepo(as)
	if err != nil {
		return nil, err
	}

	as.characters = characterRepo

	return as, nil
}

func (as *nonArenaStore) Character() database.CharacterFastRepo {
	return as.characters
}

func (as *nonArenaStore) Stop() {}

type (
	nonArenaCharacterRepo struct {
		as    *nonArenaStore
		chars *nonArenaHashedMap
		steps *model.Steps
	}

	nonArenaHashedMap struct {
		dict *sync.Map
	}

	nonArena struct {
	}
)

func newNonArenaCharacterRepo(as *nonArenaStore) (*nonArenaCharacterRepo, error) {
	chars := &nonArenaHashedMap{
		dict: &sync.Map{},
	}
	steps := &model.Steps{}
	return &nonArenaCharacterRepo{as: as, chars: chars, steps: steps}, nil
}

func (r *nonArenaCharacterRepo) GetHead(_ context.Context) *model.Steps {
	return r.steps
}

func (r *nonArenaCharacterRepo) NewStep(_ context.Context, step []byte, charID int64) {
	newStep := &model.Steps{}
	newStep.Next = nil
	newStep.Step = step
	newStep.CharID = charID
	r.steps.Next = newStep
	r.steps = newStep
}

func (r *nonArenaCharacterRepo) Update(
	ctx context.Context,
	char *character.Character,
	step []byte,
) {
	r.chars.dict.Store(char.ID, char)
	r.NewStep(ctx, step, char.ID)
}

func (r *nonArenaCharacterRepo) Wait() {}

func (r *nonArenaCharacterRepo) Disconnect(
	ctx context.Context,
	ch *character.Character,
) {
	stepDTO := dto.WSCharacterActionResponse{
		Char:   *ch,
		Action: character.Disconnect,
	}
	step, err := jsoniter.Marshal(stepDTO)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return
	}
	r.chars.dict.Delete(ch.ID)
	r.NewStep(ctx, step, ch.ID)
}

func (r *nonArenaCharacterRepo) createArenaChar(_ context.Context, char *character.Character) *character.Character {
	return char
}

func (r *nonArenaCharacterRepo) Connect(
	ctx context.Context,
	char *character.Character,
) *character.Character {

	nonArenaChar := r.createArenaChar(ctx, char)

	stepDTO := dto.WSCharacterActionResponse{
		Char:   *nonArenaChar,
		Action: character.Connect,
	}
	step, err := jsoniter.Marshal(stepDTO)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return nil
	}

	r.Update(ctx, nonArenaChar, step)
	return nonArenaChar
}

func (r *nonArenaCharacterRepo) ArenaGetAll(
	_ context.Context,
	char *character.Character,
) []character.Character {
	counter := uint32(0)
	chars := make([]character.Character, 0, 100)

	r.chars.dict.Range(
		func(k, v any) bool {
			nonArenaChar, ok := v.(*character.Character)
			if !ok || nonArenaChar == nil {
				return false
			}
			if nonArenaChar.ID != char.ID {
				chars = append(chars, *nonArenaChar)
				atomic.AddUint32(&counter, 1)
			}
			return true
		},
	)
	return chars[:counter]
}

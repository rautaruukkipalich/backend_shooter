package arenadb

import (
	"sync"
)

type ISyncMap interface {
	Get(int64) (any, bool)
	Store(int64, any)
	Delete(int64)
	Range(func(any, any) bool)
}

type SyncMap struct {
	data map[int64]any
	sync.RWMutex
}

func (m *SyncMap) Get(id int64) (any, bool) {
	m.RLock()
	defer m.RUnlock()
	v, ok := m.data[id]
	return v, ok
}

func (m *SyncMap) Store(id int64, v any) {
	m.Lock()
	defer m.Unlock()
	m.data[id] = v
}

func (m *SyncMap) Delete(id int64) {
	m.Lock()
	defer m.Unlock()
	delete(m.data, id)
}

func (m *SyncMap) Range(fn func(any, any) bool) {
	m.RLock()
	defer m.RUnlock()
	for k := range m.data {
		fn(k, m.data[k])
	}
}

package arenadb

import (
	"arena"
	"backend_shooter/internal/model/character"
	"context"
	"fmt"
	"testing"
)

func Benchmark_CreateArenaChar(b *testing.B) {
	as := &ArenaStore{
		store: arena.NewArena(),
	}
	charRepo, _ := NewCharacterRepo(as)
	defer as.Stop()

	ctx := context.Background()

	char := character.New("testChar", 1)
	char.ID = 1

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	for _, count := range tcs {
		b.Run(fmt.Sprint("arena create char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					_ = charRepo.createArenaChar(ctx, char)
				}
			}
		})
	}
}

func Benchmark_CreateNonArenaChar(b *testing.B) {
	s := &nonArenaStore{
		store: &nonArena{},
	}
	charRepo, _ := newNonArenaCharacterRepo(s)
	defer s.Stop()

	ctx := context.Background()

	char := character.New("testChar", 1)
	char.ID = 1

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	for _, count := range tcs {
		b.Run(fmt.Sprint("nonArena create char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					_ = charRepo.createArenaChar(ctx, char)
				}
			}
		})
	}
}

func Benchmark_CreateMemChar(b *testing.B) {

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	for _, count := range tcs {
		b.Run(fmt.Sprint("mem create char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					_ = character.New("testChar2", 1)
				}
			}
		})
	}
}

func Benchmark_ArenaEditChar(b *testing.B) {
	as, _ := New()
	defer as.Stop()

	ctx := context.Background()

	char := character.New("testChar", 1)
	char.ID = 1
	arenaChar := as.Character().Connect(ctx, char)

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	position := &character.Position{
		X: character.Point{Int: 1, Dec: 1},
		Y: character.Point{Int: 2, Dec: 2},
		Z: character.Point{Int: 3, Dec: 3},
	}

	for _, count := range tcs {
		b.Run(fmt.Sprint("arena move char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					arenaChar.Move(position)
				}
			}
		})
	}
}

func Benchmark_MemEditChar(b *testing.B) {
	char := character.New("testChar", 1)
	char.ID = 1

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	position := &character.Position{
		X: character.Point{Int: 1, Dec: 1},
		Y: character.Point{Int: 2, Dec: 2},
		Z: character.Point{Int: 3, Dec: 3},
	}

	for _, count := range tcs {
		b.Run(fmt.Sprint("mem move char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					char.Move(position)
				}
			}
		})
	}
}

func Benchmark_NonArenaEditChar(b *testing.B) {
	s, _ := newNonArenaStore()
	defer s.Stop()

	ctx := context.Background()

	char := character.New("testChar", 1)
	char.ID = 1
	arenaChar := s.Character().Connect(ctx, char)

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	position := &character.Position{
		X: character.Point{Int: 1, Dec: 1},
		Y: character.Point{Int: 2, Dec: 2},
		Z: character.Point{Int: 3, Dec: 3},
	}

	for _, count := range tcs {
		b.Run(fmt.Sprint("nonArena move char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					arenaChar.Move(position)
				}
			}
		})
	}
}

func Benchmark_ArenaGetAll(b *testing.B) {
	as, _ := New()
	defer as.Stop()

	ctx := context.Background()

	for i := 0; i < 1000; i++ {
		name := fmt.Sprintf("char%d", i)
		char := character.New(name, 1)
		char.ID = int64(i)
		_ = as.Character().Connect(ctx, char)
	}

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	testChar := character.New("testChar", 1)

	for _, count := range tcs {
		b.Run(fmt.Sprint("arena getAll char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					as.Character().ArenaGetAll(ctx, testChar)
				}
			}
		})
	}
}

func Benchmark_NonArenaGetAll(b *testing.B) {
	s, _ := newNonArenaStore()
	defer s.Stop()

	ctx := context.Background()

	for i := 0; i < 1000; i++ {
		name := fmt.Sprintf("char%d", i)
		char := character.New(name, 1)
		char.ID = int64(i)
		_ = s.Character().Connect(ctx, char)
	}

	tcs := []int{1, 10, 100, 1_000, 10_000, 100_000}

	testChar := character.New("testChar", 1)

	for _, count := range tcs {
		b.Run(fmt.Sprint("nonArena getAll char. count=", count), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				for j := 1; j < count; j++ {
					s.Character().ArenaGetAll(ctx, testChar)
				}
			}
		})
	}
}

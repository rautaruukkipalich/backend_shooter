package arenadb

import (
	"arena"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/pkg/logger"
	"context"
	jsoniter "github.com/json-iterator/go"
	"sync"
	"sync/atomic"
)

type (
	CharacterRepo struct {
		as    *ArenaStore
		chars *HashedMap
		steps *model.Steps
		cond  *sync.Cond
		//arenaSliceStep []*model.Steps
	}

	HashedMap struct {
		dict sync.Map
		//count int32
	}
)

func NewCharacterRepo(as *ArenaStore) (*CharacterRepo, error) {
	chars := arena.New[HashedMap](as.store)
	steps := arena.New[model.Steps](as.store)
	cond := &sync.Cond{L: &sync.Mutex{}}
	return &CharacterRepo{as: as, chars: chars, steps: steps, cond: cond}, nil
}

func (r *CharacterRepo) GetHead(_ context.Context) *model.Steps {
	return r.steps
}

func (r *CharacterRepo) NewStep(_ context.Context, step []byte, charID int64) {
	newStep := arena.New[model.Steps](r.as.store)
	newStep.Next = nil
	newStep.Step = step
	newStep.CharID = charID
	r.steps.Next = newStep
	r.steps = newStep
}

func (r *CharacterRepo) Wait() {
	r.cond.L.Lock()
	defer r.cond.L.Unlock()
	r.cond.Wait()
}

func (r *CharacterRepo) Update(
	ctx context.Context,
	char *character.Character,
	step []byte,
) {
	r.chars.dict.Store(char.ID, char)
	r.NewStep(ctx, step, char.ID)
	r.cond.L.Lock()
	defer r.cond.L.Unlock()
	r.cond.Broadcast()
}

func (r *CharacterRepo) Disconnect(
	ctx context.Context,
	ch *character.Character,
) {
	stepDTO := dto.WSCharacterActionResponse{
		Char:   *ch,
		Action: character.Disconnect,
	}
	step, err := jsoniter.Marshal(stepDTO)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return
	}
	r.chars.dict.Delete(ch.ID)
	r.NewStep(ctx, step, ch.ID)
}

func (r *CharacterRepo) createArenaChar(_ context.Context, char *character.Character) *character.Character {
	arenaChar := arena.New[character.Character](r.as.store)

	arenaChar.ID = char.ID
	arenaChar.Name = char.Name
	arenaChar.UserID = char.UserID
	arenaChar.Health = char.Health
	arenaChar.Position = char.Position

	return arenaChar
}

func (r *CharacterRepo) Connect(
	ctx context.Context,
	char *character.Character,
) *character.Character {

	arenaChar := r.createArenaChar(ctx, char)

	stepDTO := dto.WSCharacterActionResponse{
		Char:   *arenaChar,
		Action: character.Connect,
	}
	step, err := jsoniter.Marshal(stepDTO)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return nil
	}

	r.Update(ctx, arenaChar, step)
	return arenaChar
}

func (r *CharacterRepo) ArenaGetAll(
	_ context.Context,
	char *character.Character,
) []character.Character {
	counter := uint32(0)
	chars := make([]character.Character, 0, 1024)

	r.chars.dict.Range(
		func(k, v any) bool {
			arenaChar, ok := v.(*character.Character)
			if !ok || arenaChar == nil {
				return false
			}
			if arenaChar.ID != char.ID {
				chars = append(chars, *arenaChar)
				atomic.AddUint32(&counter, 1)
			}
			return true
		},
	)
	return chars[:counter]
}

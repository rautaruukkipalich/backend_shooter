package database

import (
	"backend_shooter/internal/metrics"
	"context"
	"database/sql"
	"time"
)

func QueryRowContextDecorated(
	fnName string,
	stmt *sql.Stmt,
	ctx context.Context,
	args ...any,
) *sql.Row {
	start := time.Now()
	defer func() {
		metrics.ObserveHistogramTimeQueryCounter(fnName, time.Since(start))
	}()
	return stmt.QueryRowContext(ctx, args...)
}

func QueryContextDecorated(
	fnName string,
	stmt *sql.Stmt,
	ctx context.Context,
	args ...any,
) (*sql.Rows, error) {
	start := time.Now()
	defer func() {
		metrics.ObserveHistogramTimeQueryCounter(fnName, time.Since(start))
	}()
	return stmt.QueryContext(ctx, args...)
}

func ExecContextDecorated(
	fnName string,
	stmt *sql.Stmt,
	ctx context.Context,
	args ...any,
) (sql.Result, error) {
	start := time.Now()
	defer func() {
		metrics.ObserveHistogramTimeQueryCounter(fnName, time.Since(start))
	}()
	return stmt.ExecContext(ctx, args...)
}

package database

//go:generate mockery --name CharacterDB
type CharacterDB interface {
	Character() CharacterRepo
	Stop()
}

//go:generate mockery --name CharacterFastDB
type CharacterFastDB interface {
	Character() CharacterFastRepo
	Stop()
}

type CharacterFastPushDB interface {
	Character() CharacterFastPushRepo
	Stop()
}

type CharacterFastPullDB interface {
	Character() CharacterFastPullRepo
	Stop()
}

//go:generate mockery --name UserDB
type UserDB interface {
	User() UserRepo
	Stop()
}

package arena_pull_slice

import (
	"arena"
	"backend_shooter/internal/database"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/pkg/logger"
	"context"
	jsoniter "github.com/json-iterator/go"
	"sync"
	"sync/atomic"
)

type ArenaStore struct {
	store      *arena.Arena
	characters database.CharacterFastPullRepo
}

func New() (*ArenaStore, error) {
	as := &ArenaStore{store: arena.NewArena()}

	characterRepo, err := NewCharacterRepo(as, 1024)
	if err != nil {
		return nil, err
	}

	as.characters = characterRepo

	return as, nil
}

func (as *ArenaStore) Character() database.CharacterFastPullRepo {
	return as.characters
}

func (as *ArenaStore) Stop() {
	as.store.Free()
}

type Step struct {
	Data   []byte
	CharID int64
}

type (
	CharacterRepo struct {
		as *ArenaStore
		m  *HashedMap

		sl    []*model.SliceStep
		sllen int32
		head  int32

		limit int32

		mu   sync.RWMutex
		cond *sync.Cond
	}

	HashedMap struct {
		dict  sync.Map
		count int32
	}
)

func NewCharacterRepo(as *ArenaStore, limit int32) (database.CharacterFastPullRepo, error) {
	const sliceLimit = 1 << 12

	mapStore := arena.New[HashedMap](as.store)
	sliceSteps := arena.MakeSlice[*model.SliceStep](as.store, sliceLimit, sliceLimit)

	cond := &sync.Cond{L: &sync.Mutex{}}

	return &CharacterRepo{
		as:    as,
		m:     mapStore,
		sl:    sliceSteps,
		sllen: sliceLimit,
		head:  -1,
		limit: limit,
		cond:  cond,
	}, nil
}

func (r *CharacterRepo) Connect(
	ctx context.Context,
	char *character.Character,
) *character.Character {
	if r.m.count >= r.limit {
		return nil
	}
	atomic.AddInt32(&r.m.count, 1)

	arenaChar := r.createArenaChar(ctx, char)
	step, err := r.prepareBytesConnect(arenaChar, character.Connect)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return nil
	}

	r.m.dict.Store(arenaChar.ID, arenaChar)
	r.Update(ctx, arenaChar.ID, step)

	return arenaChar
}

func (r *CharacterRepo) Disconnect(
	ctx context.Context,
	arenaChar *character.Character,
) {
	atomic.AddInt32(&r.m.count, -1)
	r.m.dict.Delete(arenaChar.ID)

	step, err := r.prepareBytesConnect(arenaChar, character.Disconnect)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return
	}
	r.Update(ctx, arenaChar.ID, step)
}

func (r *CharacterRepo) ArenaGetAll(
	_ context.Context,
	charID int64,
) []character.Character {
	r.mu.RLock()
	defer r.mu.RUnlock()

	chars := make([]character.Character, 0, r.m.count-1)

	addChar := func(k, v any) bool {
		char, ok := v.(*character.Character)
		if !ok || char == nil {
			return false
		}
		if char.ID != charID {
			chars = append(chars, *char)
		}
		return true
	}

	r.m.dict.Range(addChar)
	return chars
}

func (r *CharacterRepo) NewStep(_ context.Context, data []byte, charID int64) {
	step := arena.New[model.SliceStep](r.as.store)
	step.Step = data
	step.CharID = charID

	r.mu.Lock()
	defer r.mu.Unlock()

	r.head = (r.head + 1) % r.sllen
	r.sl[r.head] = step
}

func (r *CharacterRepo) Update(
	ctx context.Context,
	charID int64,
	step []byte,
) {
	r.NewStep(ctx, step, charID)
	r.cond.L.Lock()
	defer r.cond.L.Unlock()
	r.cond.Broadcast()
}

func (r *CharacterRepo) GetUpdate(idx int32) *model.SliceStep {
	r.mu.RLock()
	defer r.mu.RUnlock()
	return r.sl[idx%r.sllen]
}

func (r *CharacterRepo) WaitGetIndex() int32 {
	r.cond.L.Lock()
	defer r.cond.L.Unlock()
	r.cond.Wait()
	return r.head
}

func (r *CharacterRepo) createArenaChar(_ context.Context, char *character.Character) *character.Character {
	arenaChar := arena.New[character.Character](r.as.store)

	arenaChar.ID = char.ID
	arenaChar.Name = char.Name
	arenaChar.UserID = char.UserID
	arenaChar.Health = char.Health
	arenaChar.Position = char.Position

	return arenaChar
}

func (r *CharacterRepo) prepareBytesConnect(ch *character.Character, act character.Action) ([]byte, error) {
	stepDTO := dto.WSCharacterActionResponse{
		Char:   *ch,
		Action: act,
	}
	return jsoniter.Marshal(stepDTO)
}

//func (r *TestCharacterRepo) GetUpdates(ctx context.Context, tail int32) (<-chan *model.SliceStep, int32) {
//	if tail == r.head {
//		return nil, tail
//	}
//
//	ch := make(chan *model.SliceStep)
//	go func() {
//		r.mu.RLock()
//		defer r.mu.RUnlock()
//		defer close(ch)
//
//		if tail < r.head {
//			for i := tail + 1; i <= r.head; i++ {
//				select {
//				case ch <- r.aSl[i]:
//				case <-ctx.Done():
//					return
//				}
//			}
//			return
//		}
//
//		//чтоб не считать каждый раз остаток от деления можно разбить на 2 цикла
//		for i := tail; i <= r.head+r.slCap; i++ {
//			select {
//			case ch <- r.aSl[i%r.slCap]:
//			case <-ctx.Done():
//				return
//			}
//		}
//		return
//	}()
//
//	return ch, r.head
//}

//
//func (r *TestCharacterRepo) getSliceSteps(tail, head int32) []*model.SliceStep {
//	if tail < head {
//		return r.aSl[tail : head+1]
//	}
//	merged := make([]*model.SliceStep, 0, r.maxChars-tail+head+1)
//	//merged := arena.MakeSlice[*model.SliceStep](r.as.store, 0, int(slCap))
//	if tail != r.slCap-1 {
//		for i := tail + 1; i < r.slCap; i++ {
//			merged = append(merged, r.aSl[i])
//		}
//	}
//	for i := tail + 1; i < r.slCap; i++ {
//		merged = append(merged, r.aSl[i])
//	}
//
//	return merged
//}

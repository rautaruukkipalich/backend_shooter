package sqldb

import (
	"backend_shooter/config"
	"backend_shooter/internal/database"
	"backend_shooter/internal/model/user"
	"database/sql"
	"fmt"
	"testing"
)

var cfg = &config.SQLDBConfig{
	URI:    "postgres://postgres:postgres@localhost:15433/shooter_test?sslmode=disable",
	Driver: "postgres",
}

func plainTestDB(t *testing.T, cfg *config.SQLDBConfig) (*DB, func(string)) {
	t.Helper()

	db, err := sql.Open(cfg.Driver, cfg.URI)

	if err != nil {
		t.Fatal(err)
	}

	if err := db.Ping(); err != nil {
		t.Fatal(err)
	}

	return &DB{db: db}, func(table string) {
		if _, err := db.Exec(
			fmt.Sprintf("TRUNCATE %s CASCADE;", table),
		); err != nil {
			t.Fatal("teardown: ", err)
		}
		_ = db.Close()
	}
}

func TestDB(t *testing.T, cfg *config.SQLDBConfig) (*DB, func(string)) {
	store, teardown := plainTestDB(t, cfg)

	userRepo, err := NewUserRepo(store)
	if err != nil {
		t.Fatal(err)
	}
	characterRepo, err := NewCharacterRepo(store)
	if err != nil {
		t.Fatal(err)
	}

	store.userRepo = userRepo
	store.characterRepo = characterRepo

	return store, teardown
}

func getTestUser() *user.User {
	return &user.User{
		ID:       1,
		Username: "test_user",
		Password: "test_password",
	}
}

var tcsCreateUserTests = []struct {
	name     string
	username string
	err      error
}{
	{
		name:     "Case #1_1 on correct username expect no error",
		username: "test_user",
		err:      nil,
	},
	{
		name:     "Case #1_2 on duplicate username expect error",
		username: "test_user",
		err:      database.ErrUserAlreadyExist,
	},
}

var tcsSaveUserTests = []struct {
	name     string
	username string
	err      error
}{
	{
		name:     "Case #1_1 on valid username expect no error",
		username: "1",
		err:      nil,
	},
	{
		name:     "Case #1_2 on duplicate username expect error ",
		username: "1",
		err:      database.ErrUserAlreadyExist,
	},
}

var tcsGetByUsernameUserTests = []struct {
	name     string
	username string
	ok       bool
	err      error
}{
	{
		name:     "Case #1_1 on correct username expect no error",
		username: "1",
		ok:       true,
		err:      nil,
	},
	{
		name:     "Case #1_2 on incorrect username expect error NotFound",
		username: "123",
		ok:       false,
		err:      database.ErrUserNotFound,
	},
}

var tcsCreateCharacterTests = []struct {
	name     string
	charName string
	err      error
}{
	{
		name:     "Case #1_1 on correct charName expect no error",
		charName: "test_char_name",
		err:      nil,
	},
	{
		name:     "Case #1_2 on duplicate charName expect error",
		charName: "test_char_name",
		err:      database.ErrCharacterAlreadyExist,
	},
}

var tcsGetByNameCharacterTests = []struct {
	name     string
	charName string
	ok       bool
	err      error
}{
	{
		name:     "Case #1_1 on correct charName expect no error",
		charName: "1",
		ok:       true,
		err:      nil,
	},
	{
		name:     "Case #1_2 on incorrect charName expect error NotFound",
		charName: "123",
		ok:       false,
		err:      database.ErrCharacterNotFound,
	},
}

var tcsGetByIDCharacterTests = []struct {
	name   string
	charID int64
	ok     bool
	err    error
}{
	{
		name:   "Case #1_1 on correct charID expect no error",
		charID: 1,
		ok:     true,
		err:    nil,
	},
	{
		name:   "Case #1_2 on incorrect charID expect error NotFound",
		charID: 124,
		ok:     false,
		err:    database.ErrCharacterNotFound,
	},
}

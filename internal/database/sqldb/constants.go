package sqldb

const (
	queryInsertUser    = "user.insertUser"
	queryGetByUsername = "user.getByUsername"
	queryUpdateUser    = "user.updateUser"

	queryGetByID          = "character.getCharacterByID"
	queryGetByName        = "character.getCharacterByName"
	queryInsertCharacter  = "character.insertCharacter"
	queryInsertPosition   = "character.insertPosition"
	queryUpdateCharacter  = "character.updateCharacter"
	queryGetCharsByUserID = "character.getCharsByUserID"
)

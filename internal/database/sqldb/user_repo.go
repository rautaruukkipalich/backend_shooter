package sqldb

import (
	"backend_shooter/internal/database"
	"backend_shooter/internal/model/user"
	"backend_shooter/resources"
	"context"
	"database/sql"
	"errors"
	"strings"
	"time"
)

type (
	UserRepo struct {
		store *DB
	}
)

func NewUserRepo(db *DB) (*UserRepo, error) {
	return &UserRepo{db}, nil
}

func (r *UserRepo) Create(ctx context.Context, u *user.User) error {
	tx, err := r.store.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.InsertUser)
	if err != nil {
		return err
	}

	err = database.QueryRowContextDecorated(
		queryInsertUser, stmt, ctx, u.Username, u.HashedPassword, time.Now().UTC(),
	).Scan(&u.ID)

	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return database.ErrUserAlreadyExist
		}
		return err
	}

	return tx.Commit()
}

func (r *UserRepo) GetByUsername(ctx context.Context, username string, u *user.User) (bool, error) {
	tx, err := r.store.db.BeginTx(ctx, nil)
	if err != nil {
		return false, err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.GetUserByUsername)
	if err != nil {
		return false, err
	}

	row := database.QueryRowContextDecorated(queryGetByUsername, stmt, ctx, username)

	if err := row.Scan(&u.ID, &u.Username, &u.HashedPassword); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, database.ErrUserNotFound
		}
		return false, err
	}

	return true, tx.Commit()
}

func (r *UserRepo) GetByID(_ context.Context, _ int64, _ *user.User) (bool, error) {
	return true, nil
}

func (r *UserRepo) Save(ctx context.Context, u *user.User) error {
	tx, err := r.store.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.UpdateUser)

	if err != nil {
		return err
	}

	_, err = database.ExecContextDecorated(queryUpdateUser, stmt, ctx, u.Username, u.HashedPassword, u.ID)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return database.ErrUserAlreadyExist
		}
		return err
	}

	return tx.Commit()
}

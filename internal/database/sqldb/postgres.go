package sqldb

import (
	"database/sql"
	"fmt"

	"backend_shooter/config"
	"backend_shooter/internal/database"

	_ "github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

type DB struct {
	db            *sql.DB
	userRepo      database.UserRepo
	characterRepo database.CharacterRepo
}

func New(cfg *config.SQLDBConfig) (*DB, error) {
	db, err := sql.Open(cfg.Driver, cfg.URI)
	if err != nil {
		return nil, fmt.Errorf("%v: %w", database.ErrConnectDB, err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("%v: %w", database.ErrConnectDB, err)
	}

	db.SetMaxOpenConns(cfg.MaxConnections)

	s := &DB{db: db}

	userRepo, err := NewUserRepo(s)
	if err != nil {
		return nil, err
	}

	characterRepo, err := NewCharacterRepo(s)
	if err != nil {
		return nil, err
	}

	s.characterRepo = characterRepo
	s.userRepo = userRepo

	return s, nil
}

func (db *DB) User() database.UserRepo {
	return db.userRepo
}

func (db *DB) Character() database.CharacterRepo {
	return db.characterRepo
}

func (db *DB) Stop() {
	_ = db.db.Close()
}

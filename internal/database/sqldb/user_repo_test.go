package sqldb

import (
	"backend_shooter/internal/model/user"
	"context"
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

func Test_NewUserRepo(t *testing.T) {
	store, teardown := plainTestDB(t, cfg)
	defer teardown("users")

	userRepo, err := NewUserRepo(store)
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, store, userRepo.store)
	assert.NotNil(t, userRepo)
	assert.NoError(t, err)
}

func Test_UserRepo_Create(t *testing.T) {
	store, teardown := TestDB(t, cfg)
	defer teardown("users")

	ctx := context.TODO()

	for _, tc := range tcsCreateUserTests {
		t.Run(tc.name, func(t *testing.T) {
			u := &user.User{Hasher: user.Hasher}
			_ = user.New(u, tc.username, "")

			err := store.User().Create(ctx, u)

			assert.Equal(t, tc.err, err)
		})
	}
}

func Test_UserRepo_Save(t *testing.T) {
	store, teardown := TestDB(t, cfg)
	defer teardown("users")

	ctx := context.TODO()

	for _, tc := range tcsSaveUserTests {
		t.Run(tc.name, func(t *testing.T) {
			name := "123test"
			u := &user.User{Hasher: user.Hasher}
			_ = user.New(u, name, "")
			_ = store.User().Create(ctx, u)

			u.Username = tc.username
			err := store.User().Save(ctx, u)

			assert.Equal(t, tc.err, err)
		})
	}
}

func Test_UserRepo_GetByUsername(t *testing.T) {
	store, teardown := TestDB(t, cfg)
	defer teardown("users")

	ctx := context.TODO()

	for i := 1; i < 10; i++ {
		u := &user.User{Hasher: user.Hasher}
		_ = user.New(u, strconv.Itoa(i), "123")
		_ = store.User().Create(ctx, u)
	}

	for _, tc := range tcsGetByUsernameUserTests {
		t.Run(tc.name, func(t *testing.T) {
			u := &user.User{Hasher: user.Hasher}
			ok, err := store.User().GetByUsername(ctx, tc.username, u)

			assert.Equal(t, tc.ok, ok)
			assert.Equal(t, tc.err, err)
		})
	}
}

package sqldb

import (
	"backend_shooter/internal/model/character"
	"context"
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

func Test_NewCharacterRepo(t *testing.T) {
	t.Run("new character repo", func(t *testing.T) {
		store, _ := plainTestDB(t, cfg)

		characterRepo, err := NewCharacterRepo(store)
		if err != nil {
			t.Fatal(err)
		}
		assert.Equal(t, store, characterRepo.store)
		assert.NotNil(t, characterRepo)
		assert.NoError(t, err)
	})
}

func Test_CharacterRepo_Create(t *testing.T) {
	store, teardown := TestDB(t, cfg)
	defer teardown("users")

	u := getTestUser()
	ctx := context.TODO()
	err := store.User().Create(ctx, u)

	for _, tc := range tcsCreateCharacterTests {
		t.Run(tc.name, func(t *testing.T) {
			char := character.New(tc.charName, u.ID)
			err = store.Character().Create(ctx, char)
			assert.Equal(t, tc.err, err)
		})
	}
}

func Test_CharacterRepo_GetByName(t *testing.T) {
	store, teardown := TestDB(t, cfg)
	defer teardown("users")

	u := getTestUser()
	ctx := context.TODO()
	_ = store.User().Create(ctx, u)

	for i := 0; i < 10; i++ {
		char := character.New(strconv.Itoa(i), u.ID)
		_ = store.Character().Create(ctx, char)
	}

	for _, tc := range tcsGetByNameCharacterTests {
		t.Run(tc.name, func(t *testing.T) {
			var char character.Character
			ok, err := store.Character().GetByName(ctx, tc.charName, &char)

			assert.Equal(t, tc.err, err)
			assert.Equal(t, tc.ok, ok)
		})
	}
}

func Test_CharacterRepo_GetByID(t *testing.T) {
	store, teardown := TestDB(t, cfg)
	defer teardown("users")

	u := getTestUser()
	ctx := context.TODO()
	_ = store.User().Create(ctx, u)

	var lastID int64
	depth := 10

	for i := 0; i < depth; i++ {
		char := character.New(strconv.Itoa(i), u.ID)
		_ = store.Character().Create(ctx, char)
		lastID = char.ID
	}

	virtualZero := lastID - int64(depth)

	for _, tc := range tcsGetByIDCharacterTests {
		t.Run(tc.name, func(t *testing.T) {
			var char character.Character
			ok, err := store.Character().GetByID(ctx, virtualZero+tc.charID, &char)

			assert.Equal(t, tc.err, err)
			assert.Equal(t, tc.ok, ok)
		})
	}
}

func Test_CharacterRepo_Save(t *testing.T) {
	store, teardown := TestDB(t, cfg)
	defer teardown("users")

	ctx := context.TODO()

	chName := "testChar"

	u := getTestUser()

	if err := store.User().Create(ctx, u); err != nil {
		t.Fatal(err)
	}

	char := character.New(chName, u.ID)

	if err := store.Character().Create(ctx, char); err != nil {
		t.Fatal(err)
	}

	move := character.Position{X: character.Point{Int: 5, Dec: 6}}

	for i := 1; i < 3; i++ {
		char.Move(&move)
		if err := store.Character().Save(ctx, *char); err != nil {
			t.Fatal(err)
		}
	}

	var char2 character.Character
	ok, err := store.Character().GetByName(ctx, chName, &char2)

	assert.NoError(t, err)
	assert.True(t, ok)
	assert.Equal(t, char.Name, char2.Name)
	assert.Equal(t, char.Position, char2.Position)
	assert.Equal(t, char2.Position, character.Position{X: character.Point{Int: 11, Dec: 2}})
}

package sqldb

import (
	"backend_shooter/internal/database"
	"backend_shooter/internal/model/character"
	"backend_shooter/resources"
	"context"
	"database/sql"
	"errors"
	"strings"
	"sync/atomic"
	"time"
)

type (
	CharacterRepo struct {
		store *DB
	}
)

func NewCharacterRepo(db *DB) (*CharacterRepo, error) {
	return &CharacterRepo{db}, nil
}

func (c *CharacterRepo) GetByID(ctx context.Context, charID int64, ch *character.Character) (bool, error) {
	tx, err := c.store.db.BeginTx(ctx, nil)
	if err != nil {
		return false, err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.GetCharacterByID)
	if err != nil {
		return false, err
	}

	row := database.QueryRowContextDecorated(queryGetByID, stmt, ctx, charID)

	if err := row.Scan(
		&ch.ID, &ch.Name,
		&ch.UserID, &ch.Health,
		&ch.Position.X.Int, &ch.Position.X.Dec,
		&ch.Position.Y.Int, &ch.Position.Y.Dec,
		&ch.Position.Z.Int, &ch.Position.Z.Dec,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, database.ErrCharacterNotFound
		}
		return false, err
	}

	return true, tx.Commit()
}

func (c *CharacterRepo) GetByName(ctx context.Context, name string, ch *character.Character) (bool, error) {
	tx, err := c.store.db.BeginTx(ctx, nil)
	if err != nil {
		return false, err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.GetCharacterByName)
	if err != nil {
		return false, err
	}

	row := database.QueryRowContextDecorated(queryGetByName, stmt, ctx, name)

	if err := row.Scan(
		&ch.ID, &ch.Name,
		&ch.UserID, &ch.Health,
		&ch.Position.X.Int, &ch.Position.X.Dec,
		&ch.Position.Y.Int, &ch.Position.Y.Dec,
		&ch.Position.Z.Int, &ch.Position.Z.Dec,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, database.ErrCharacterNotFound
		}
		return false, err
	}

	return true, tx.Commit()

}

func (c *CharacterRepo) Create(ctx context.Context, ch *character.Character) error {

	tx, err := c.store.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.InsertCharacter)
	if err != nil {
		return err
	}

	if err = database.QueryRowContextDecorated(queryInsertCharacter, stmt, ctx, ch.Name, ch.UserID, ch.Health, time.Now().UTC()).Scan(&ch.ID); err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return database.ErrCharacterAlreadyExist
		}
		return err
	}

	stmt, err = tx.Prepare(resources.InsertPosition)
	if err != nil {
		return err
	}

	_, err = database.ExecContextDecorated(
		queryInsertPosition, stmt, ctx, ch.ID,
		ch.Position.X.Int,
		ch.Position.X.Dec,
		ch.Position.Y.Int,
		ch.Position.Y.Dec,
		ch.Position.Z.Int,
		ch.Position.Z.Dec,
		time.Now().UTC(),
	)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (c *CharacterRepo) Save(ctx context.Context, ch character.Character) error {

	tx, err := c.store.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.UpdateCharacter)
	if err != nil {
		return err
	}

	_, err = database.ExecContextDecorated(queryUpdateCharacter, stmt, ctx, ch.Health, ch.ID)
	if err != nil {
		return err
	}

	stmt, err = tx.Prepare(resources.InsertPosition)
	if err != nil {
		return err
	}

	_, err = database.ExecContextDecorated(
		queryInsertPosition, stmt, ctx, ch.ID,
		ch.Position.X.Int,
		ch.Position.X.Dec,
		ch.Position.Y.Int,
		ch.Position.Y.Dec,
		ch.Position.Z.Int,
		ch.Position.Z.Dec,
		time.Now().UTC())
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (c *CharacterRepo) GetByUserID(ctx context.Context, userID int64) ([]character.Character, error) {
	chars := make([]character.Character, 0, 10)
	var charsCount int32

	tx, err := c.store.db.BeginTx(ctx, nil)
	if err != nil {
		return chars[:charsCount], err
	}
	defer func() {
		_ = tx.Rollback()
	}()

	stmt, err := tx.Prepare(resources.GetCharactersByUserID)
	if err != nil {
		return chars[:charsCount], err
	}

	rows, err := database.QueryContextDecorated(queryGetCharsByUserID, stmt, ctx, userID)
	if err != nil || rows == nil {
		return chars[:charsCount], err
	}

	for rows.Next() {
		var char character.Character
		if err := rows.Scan(&char.ID, &char.Name, &char.UserID, &char.Health); err != nil {
			return chars[:charsCount], err
		}
		chars = append(chars, char)
		atomic.AddInt32(&charsCount, 1)
	}

	return chars[:charsCount], nil
}

func (c *CharacterRepo) Delete(_ context.Context, _ *character.Character) error {
	return nil
}

// Code generated by mockery v2.43.2. DO NOT EDIT.

package mocks

import (
	database "backend_shooter/internal/database"

	mock "github.com/stretchr/testify/mock"
)

// UserDB is an autogenerated mock type for the UserDB type
type UserDB struct {
	mock.Mock
}

// Stop provides a mock function with given fields:
func (_m *UserDB) Stop() {
	_m.Called()
}

// User provides a mock function with given fields:
func (_m *UserDB) User() database.UserRepo {
	ret := _m.Called()

	if len(ret) == 0 {
		panic("no return value specified for User")
	}

	var r0 database.UserRepo
	if rf, ok := ret.Get(0).(func() database.UserRepo); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(database.UserRepo)
		}
	}

	return r0
}

// NewUserDB creates a new instance of UserDB. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewUserDB(t interface {
	mock.TestingT
	Cleanup(func())
}) *UserDB {
	mock := &UserDB{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}

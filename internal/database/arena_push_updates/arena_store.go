package arena_push_updates

import (
	"arena"
	"backend_shooter/internal/database"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/pkg/logger"
	"context"
	jsoniter "github.com/json-iterator/go"
	"sync"
	"sync/atomic"
)

type ArenaStore struct {
	store      *arena.Arena
	characters database.CharacterFastPushRepo
}

func New() (*ArenaStore, error) {
	as := &ArenaStore{store: arena.NewArena()}

	characterRepo, err := NewCharacterRepo(as, 1024)
	if err != nil {
		return nil, err
	}

	as.characters = characterRepo

	return as, nil
}

func (as *ArenaStore) Character() database.CharacterFastPushRepo {
	return as.characters
}

func (as *ArenaStore) Stop() {
	as.store.Free()
}

type Step struct {
	Data   []byte
	CharID int64
}

type (
	CharacterRepo struct {
		as       *ArenaStore
		chars    *HashedMap
		maxChars int32
		sema     chan struct{}
		//mu       sync.RWMutex
		//cond     *sync.Cond
		//aSl      []*model.SliceStep
		//head     int32
		//tail     int32
		//slCap    int32

	}

	HashedMap struct {
		dict  sync.Map
		count int32
	}

	CharConn struct {
		char *character.Character
		ch   chan *model.SliceStep
	}
)

func NewCharacterRepo(as *ArenaStore, maxChars int32) (database.CharacterFastPushRepo, error) {
	chars := arena.New[HashedMap](as.store)
	sema := make(chan struct{}, 10000)
	return &CharacterRepo{
		as:       as,
		chars:    chars,
		maxChars: maxChars,
		sema:     sema,
	}, nil
}

func (r *CharacterRepo) NewStep(ctx context.Context, data []byte, charID int64) {
	//step := arena.New[model.SliceStep](r.as.store)
	//step.Step = data
	//step.CharID = charID

	//r.head = (r.head + 1) % r.slCap
	//r.aSl[r.head] = step

	//r.mu.RLock()
	//defer r.mu.RUnlock()

	select {
	case <-ctx.Done():
		return
	case r.sema <- struct{}{}:
		step := arena.New[model.SliceStep](r.as.store)
		step.Step = data
		step.CharID = charID

		r.chars.dict.Range(
			func(k, v any) bool {
				char, ok := v.(*CharConn)
				if !ok || char == nil {
					return false
				}

				select {
				case char.ch <- step:
				default:
				}

				return true
			},
		)
		<-r.sema
	}
}

func (r *CharacterRepo) Update(
	ctx context.Context,
	charID int64,
	step []byte,
) {
	r.NewStep(ctx, step, charID)
}

func (r *CharacterRepo) Disconnect(
	ctx context.Context,
	arenaChar *character.Character,
) {
	r.chars.dict.Delete(arenaChar.ID)
	atomic.AddInt32(&r.chars.count, -1)
	step, err := r.prepareBytesConnect(arenaChar, character.Disconnect)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return
	}
	r.Update(ctx, arenaChar.ID, step)
}

func (r *CharacterRepo) Connect(
	ctx context.Context,
	char *character.Character,
	ch chan *model.SliceStep,
) *character.Character {
	if r.chars.count >= r.maxChars {
		return nil
	}

	arenaChar := r.createArenaChar(ctx, char)
	step, err := r.prepareBytesConnect(arenaChar, character.Connect)
	if err != nil {
		logger.FromContext(ctx).Error(err.Error())
		return nil
	}
	atomic.AddInt32(&r.chars.count, 1)

	charStruct := arena.New[CharConn](r.as.store)
	charStruct.char = arenaChar
	charStruct.ch = ch

	r.chars.dict.Store(arenaChar.ID, charStruct)
	r.Update(ctx, arenaChar.ID, step)
	return arenaChar
}

func (r *CharacterRepo) ArenaGetAll(
	_ context.Context,
	char *character.Character,
) []character.Character {
	count := r.chars.count
	chars := make([]character.Character, 0, count)

	r.chars.dict.Range(
		func(k, v any) bool {
			ch, ok := v.(*CharConn)
			if !ok || ch == nil {
				return false
			}
			if ch.char.ID != char.ID {
				chars = append(chars, *ch.char)
			}
			return true
		},
	)
	return chars[:count-1]
}

func (r *CharacterRepo) Wait() {
}

func (r *CharacterRepo) createArenaChar(_ context.Context, char *character.Character) *character.Character {
	arenaChar := arena.New[character.Character](r.as.store)

	arenaChar.ID = char.ID
	arenaChar.Name = char.Name
	arenaChar.UserID = char.UserID
	arenaChar.Health = char.Health
	arenaChar.Position = char.Position

	return arenaChar
}

func (r *CharacterRepo) prepareBytesConnect(ch *character.Character, act character.Action) ([]byte, error) {
	stepDTO := dto.WSCharacterActionResponse{
		Char:   *ch,
		Action: act,
	}
	return jsoniter.Marshal(stepDTO)
}

//func (r *TestCharacterRepo) GetUpdates(ctx context.Context, tail int32) (<-chan *model.SliceStep, int32) {
//	if tail == r.head {
//		return nil, tail
//	}
//
//	ch := make(chan *model.SliceStep)
//	go func() {
//		r.mu.RLock()
//		defer r.mu.RUnlock()
//		defer close(ch)
//
//		if tail < r.head {
//			for i := tail + 1; i <= r.head; i++ {
//				select {
//				case ch <- r.aSl[i]:
//				case <-ctx.Done():
//					return
//				}
//			}
//			return
//		}
//
//		//чтоб не считать каждый раз остаток от деления можно разбить на 2 цикла
//		for i := tail; i <= r.head+r.slCap; i++ {
//			select {
//			case ch <- r.aSl[i%r.slCap]:
//			case <-ctx.Done():
//				return
//			}
//		}
//		return
//	}()
//
//	return ch, r.head
//}

//
//func (r *TestCharacterRepo) getSliceSteps(tail, head int32) []*model.SliceStep {
//	if tail < head {
//		return r.aSl[tail : head+1]
//	}
//	merged := make([]*model.SliceStep, 0, r.maxChars-tail+head+1)
//	//merged := arena.MakeSlice[*model.SliceStep](r.as.store, 0, int(slCap))
//	if tail != r.slCap-1 {
//		for i := tail + 1; i < r.slCap; i++ {
//			merged = append(merged, r.aSl[i])
//		}
//	}
//	for i := tail + 1; i < r.slCap; i++ {
//		merged = append(merged, r.aSl[i])
//	}
//
//	return merged
//}

package database

import (
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/internal/model/user"
	"context"
)

type CharacterRepoGetter interface {
	GetByID(context.Context, int64, *character.Character) (bool, error)
	GetByName(context.Context, string, *character.Character) (bool, error)
	GetByUserID(context.Context, int64) ([]character.Character, error)
}

type CharactersRepoSetter interface {
	Create(context.Context, *character.Character) error
	Save(context.Context, character.Character) error
	Delete(context.Context, *character.Character) error
}

type UserRepoGetter interface {
	GetByID(context.Context, int64, *user.User) (bool, error)
	GetByUsername(context.Context, string, *user.User) (bool, error)
}

type UserRepoSetter interface {
	Create(context.Context, *user.User) error
	Save(context.Context, *user.User) error
}

//go:generate mockery --name UserRepo
type UserRepo interface {
	UserRepoGetter
	UserRepoSetter
}

//go:generate mockery --name CharacterRepo
type CharacterRepo interface {
	CharacterRepoGetter
	CharactersRepoSetter
}

//go:generate mockery --name CharacterFastRepo
type CharacterFastRepo interface {
	ArenaGetAll(context.Context, *character.Character) []character.Character
	Connect(context.Context, *character.Character) *character.Character
	Disconnect(context.Context, *character.Character)
	GetHead(context.Context) *model.Steps
	Update(context.Context, *character.Character, []byte)
	Wait()
}

type CharacterFastPushRepo interface {
	ArenaGetAll(context.Context, *character.Character) []character.Character
	Connect(context.Context, *character.Character, chan *model.SliceStep) *character.Character
	Disconnect(context.Context, *character.Character)
	Update(context.Context, int64, []byte)
}

type CharacterFastPullRepo interface {
	ArenaGetAll(context.Context, int64) []character.Character
	Connect(context.Context, *character.Character) *character.Character
	Disconnect(context.Context, *character.Character)
	Update(context.Context, int64, []byte)
	WaitGetIndex() int32
	GetUpdate(idx int32) *model.SliceStep
}

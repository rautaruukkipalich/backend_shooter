package character

import (
	"math"
	"sync"
	"sync/atomic"
)

type (
	Character struct {
		ID       int64    `json:"id"`
		Name     string   `json:"name"`
		UserID   int64    `json:"user_id"`
		Position Position `json:"position"`
		Health   int      `json:"health"`
	}

	Position struct {
		X Point `json:"x"`
		Y Point `json:"y"`
		Z Point `json:"z"`
	}

	Point struct {
		Int int32 `json:"int"`
		Dec int32 `json:"dec"`
	}

	Axis struct {
		posInt  *int32
		moveInt *int32
		posDec  *int32
		moveDec *int32
	}

	Action string
)

const accuracy = 10

func New(name string, idx int64) *Character {
	return &Character{
		Name:   name,
		UserID: idx,
		Health: 100,
		Position: Position{
			Point{0, 0},
			Point{0, 0},
			Point{0, 0},
		},
	}
}

func (p *Character) Move(move *Position) {
	axes := p.getAxis(move)

	var wg sync.WaitGroup
	wg.Add(len(axes))

	for _, axis := range axes {
		go func() {
			defer wg.Done()
			posMoveHandle(axis.posInt, axis.moveInt, axis.posDec, axis.moveDec)
		}()
	}

	wg.Wait()
}

func (p *Character) MoveTo(move *Position) {
	axes := p.getAxis(move)

	var wg sync.WaitGroup
	wg.Add(len(axes))

	for _, axis := range axes {
		go func() {
			defer wg.Done()
			posSkipHandle(axis.posInt, axis.moveInt, axis.posDec, axis.moveDec)
		}()
	}

	wg.Wait()
}

//func (p *Character) Online() {
//	p.IsOnline = true
//}
//
//func (p *Character) Offline() {
//	p.IsOnline = false
//}

func (p *Character) getAxis(move *Position) []Axis {
	return []Axis{
		{&p.Position.X.Int, &move.X.Int, &p.Position.X.Dec, &move.X.Dec},
		{&p.Position.Y.Int, &move.Y.Int, &p.Position.Y.Dec, &move.Y.Dec},
		{&p.Position.Z.Int, &move.Z.Int, &p.Position.Z.Dec, &move.Z.Dec},
	}
}

func posMoveHandle(Int, moveInt, Dec, moveDec *int32) {
	var deltaInt = *moveDec / accuracy
	var deltaDec = *moveDec % accuracy

	atomic.AddInt32(Int, *moveInt+deltaInt)
	atomic.AddInt32(Dec, deltaDec)
	posCollisionHandle(Int, Dec)
}

func posSkipHandle(Int, moveInt, Dec, moveDec *int32) {
	var deltaInt = *moveDec / accuracy
	var deltaDec = *moveDec % accuracy

	atomic.StoreInt32(Int, *moveInt+deltaInt)
	atomic.StoreInt32(Dec, deltaDec)
	posCollisionHandle(Int, Dec)
}

func posCollisionHandle(Int *int32, Dec *int32) {

	var deltaInt = *Dec / accuracy

	if *Dec <= -accuracy {
		atomic.AddInt32(Int, deltaInt)
		atomic.AddInt32(Dec, accuracy)
	}
	if *Dec >= accuracy {
		atomic.AddInt32(Int, deltaInt)
		atomic.AddInt32(Dec, -accuracy)
	}

	if *Int < 0 && *Dec > 0 {
		atomic.AddInt32(Int, 1)
		atomic.AddInt32(Dec, -accuracy)
	}

	if *Int > 0 && *Dec < 0 {
		atomic.AddInt32(Int, -1)
		atomic.AddInt32(Dec, accuracy)
	}

}

const maxShowDistance = 100

func (p *Character) CheckShowDistanceToEnemy(en *Character) bool {
	//d^2 = a^2 + b^2 + c^2
	x := getParallelepipedSide(p.Position.X, en.Position.X)
	y := getParallelepipedSide(p.Position.Y, en.Position.Y)
	z := getParallelepipedSide(p.Position.Z, en.Position.Z)

	res := math.Sqrt(x*x + y*y + z*z)
	return res < maxShowDistance
}

func getParallelepipedSide(p, en Point) float64 {
	abs := math.Abs(
		float64(
			(p.Int*accuracy + p.Dec) - (en.Int*accuracy + en.Dec),
		),
	)
	return abs / accuracy
}

package character

const (
	MoveAction   Action = "move"
	MoveToAction Action = "moveto"
	Connect      Action = "connect"
	Disconnect   Action = "disconnect"
)

package character

func GetEnemies() []Character {
	return []Character{
		{Name: "character 1", Health: 100},
		{Name: "character 2", Health: 75},
		{Name: "character 3", Health: 50},
		{Name: "character 4", Health: 25},
	}
}

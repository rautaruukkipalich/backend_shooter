package character

import (
	"backend_shooter/internal/model/user"
	"testing"
)

func testUser() *user.User {
	return &user.User{
		ID:       1,
		Username: "username",
		Password: "password",
	}
}

func newTestChar(t *testing.T) *Character {
	t.Helper()
	return New("testChar", testUser().ID)
}

var tcsCalculateMove = []struct {
	name string
	move Position
	res  Position
}{
	{
		name: "move 1",
		move: Position{X: Point{0, 2}},
		res:  Position{Point{0, 2}, Point{}, Point{}},
	},
	{
		name: "move 2",
		move: Position{X: Point{0, 9}},
		res:  Position{Point{1, 1}, Point{}, Point{}},
	},
	{
		name: "move 3",
		move: Position{X: Point{0, -45}},
		res:  Position{Point{-3, -4}, Point{}, Point{}},
	},
	{
		name: "move 4",
		move: Position{X: Point{2, 31}},
		res:  Position{Point{1, 7}, Point{}, Point{}},
	},
	{
		name: "move 5",
		move: Position{X: Point{-1, -7}},
		res:  Position{Point{0, 0}, Point{}, Point{}},
	},
	{
		name: "move 6",
		move: Position{X: Point{5, -6}},
		res:  Position{Point{4, 4}, Point{}, Point{}},
	},
	{
		name: "move 7",
		move: Position{X: Point{7, -12}},
		res:  Position{Point{10, 2}, Point{}, Point{}},
	},
	{
		name: "move 8",
		move: Position{X: Point{-8, -3}},
		res:  Position{Point{1, 9}, Point{}, Point{}},
	},
	{
		name: "move 9",
		move: Position{X: Point{-4, -12}},
		res:  Position{Point{-3, -3}, Point{}, Point{}},
	},
}

var tcsSkipMove = []struct {
	name string
	move Position
	res  Position
}{
	{
		name: "move 1",
		move: Position{Point{1, 2}, Point{-1, 4}, Point{7, 9}},
		res:  Position{Point{1, 2}, Point{0, -6}, Point{7, 9}},
	},
	{
		name: "move 2",
		move: Position{Point{11, 24}, Point{-1, 2}, Point{7, 9}},
		res:  Position{Point{13, 4}, Point{0, -8}, Point{7, 9}},
	},
	{
		name: "move 3",
		move: Position{Point{11, 24}, Point{-1, 2}, Point{74, 20}},
		res:  Position{Point{13, 4}, Point{0, -8}, Point{76, 0}},
	},
}

//var tcsEnemyDistance = []struct {
//	name  string
//	enemy Character
//	res   bool
//}{
//	{
//		name: "Enemy 1",
//		enemy: Character{
//			ID:     1,
//			Name:   "Enemy 1",
//			UserID: 1,
//			Health: 100,
//			Position: Position{
//				X: Point{Int: 0, Dec: 0},
//				Y: Point{Int: 0, Dec: 0},
//				Z: Point{Int: 0, Dec: 0},
//			},
//		},
//		res: true,
//	},
//	{
//		name: "Enemy 2",
//		enemy: Character{
//			ID:     2,
//			Name:   "Enemy 2",
//			UserID: 2,
//			Health: 100,
//			Position: Position{
//				X: Point{Int: 100, Dec: 0},
//				Y: Point{Int: 50, Dec: 0},
//				Z: Point{Int: 10, Dec: 0},
//			},
//		},
//		res: false,
//	},
//	{
//		name: "Enemy 3",
//		enemy: Character{
//			ID:     3,
//			Name:   "Enemy 3",
//			UserID: 3,
//			Health: 100,
//			Position: Position{
//				X: Point{Int: -50, Dec: 0},
//				Y: Point{Int: -50, Dec: 0},
//				Z: Point{Int: -50, Dec: 0},
//			},
//		},
//		res: true,
//	},
//	{
//		name: "Enemy 4",
//		enemy: Character{
//			ID:     4,
//			Name:   "Enemy 4",
//			UserID: 4,
//			Health: 100,
//			Position: Position{
//				X: Point{Int: -50, Dec: 0},
//				Y: Point{Int: 100, Dec: 0},
//				Z: Point{Int: -50, Dec: 0},
//			},
//		},
//		res: false,
//	},
//	{
//		name: "Enemy 5",
//		enemy: Character{
//			ID:     5,
//			Name:   "Enemy 5",
//			UserID: 5,
//			Health: 100,
//			Position: Position{
//				X: Point{Int: 0, Dec: 0},
//				Y: Point{Int: 100, Dec: 0},
//				Z: Point{Int: 0, Dec: 0},
//			},
//		},
//		res: false,
//	},
//	{
//		name: "Enemy 6",
//		enemy: Character{
//			ID:     6,
//			Name:   "Enemy 6",
//			UserID: 6,
//			Health: 100,
//			Position: Position{
//				X: Point{Int: 0, Dec: 0},
//				Y: Point{Int: 99, Dec: 9},
//				Z: Point{Int: 0, Dec: 0},
//			},
//		},
//		res: true,
//	},
//}

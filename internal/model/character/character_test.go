package character

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_MoveCharacter(t *testing.T) {
	char := newTestChar(t)

	for _, tc := range tcsCalculateMove {
		t.Run(tc.name, func(t *testing.T) {
			char.Move(&tc.move)
			assert.Equal(t, tc.res, char.Position, tc.name)
		})
	}
}

func Test_SkipCharacter(t *testing.T) {
	char := newTestChar(t)

	for _, tc := range tcsSkipMove {
		t.Run(tc.name, func(t *testing.T) {
			char.MoveTo(&tc.move)
			assert.Equal(t, tc.res, char.Position, tc.name)
		})
	}
}

//func Test_CheckShowDistanceToEnemy(t *testing.T) {
//	char := newTestChar(t)
//
//	for _, tc := range tcsEnemyDistance {
//		t.Run(tc.name, func(t *testing.T) {
//			ok := char.CheckShowDistanceToEnemy(&tc.enemy)
//			assert.Equal(t, tc.res, ok)
//		})
//	}
//}

package model

type (
	Steps struct {
		Step   []byte
		CharID int64
		Next   *Steps
	}

	SliceStep struct {
		Step   []byte
		CharID int64
	}
)

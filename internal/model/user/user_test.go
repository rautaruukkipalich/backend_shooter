package user

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_User_BeforeCreate(t *testing.T) {
	u := TUser(t)
	assert.NoError(t, u.BeforeCreate())
	assert.NotEmpty(t, u.HashedPassword)
}

func Test_User_ValidateUsername(t *testing.T) {
	for _, tc := range getUsernameTestCases(t) {
		t.Run(tc.name, func(t *testing.T) {
			if tc.isValid {
				assert.NoError(t, tc.u().Validate())
			} else {
				assert.Error(t, tc.u().Validate())
			}
		})
	}
}

func Test_User_ValidatePassword(t *testing.T) {
	for _, tc := range getPasswordTestCases(t) {
		t.Run(tc.name, func(t *testing.T) {
			if tc.isValid {
				assert.NoError(t, tc.u().Validate())
			} else {
				assert.Error(t, tc.u().Validate())
			}
		})
	}
}

func Test_User_Validate(t *testing.T) {
	for _, tc := range getTestCases(t) {
		t.Run(tc.name, func(t *testing.T) {
			if tc.isValid {
				assert.NoError(t, tc.u().Validate())
			} else {
				assert.Error(t, tc.u().Validate())
			}
		})
	}
}

package user

import "testing"

func TUser(t *testing.T) *User {
	t.Helper()
	return &User{
		Username: "testuser",
		Password: "testpassword",
		Hasher:   Hasher,
	}
}

type TestUser struct {
	name    string
	u       func() *User
	isValid bool
}

func getTestCases(t *testing.T) []TestUser {
	return append(getUsernameTestCases(t), getPasswordTestCases(t)...)
}

func getPasswordTestCases(t *testing.T) []TestUser {
	return []TestUser{
		{
			name: "on empty password expect an error",
			u: func() *User {
				u := TUser(t)
				u.Password = ""
				return u
			},
			isValid: false,
		},
		{
			name: "on password with spaces expect an error",
			u: func() *User {
				u := TUser(t)
				u.Password = "aaa bbb"
				return u
			},
			isValid: false,
		},
		{
			name: "on password with letters and numbers expect no error",
			u: func() *User {
				u := TUser(t)
				u.Password = "aaa21331"
				return u
			},
			isValid: true,
		},
		{
			name: "on password with syblols expect an error",
			u: func() *User {
				u := TUser(t)
				u.Password = "aaa;bbb"
				return u
			},
			isValid: false,
		},
	}
}

func getUsernameTestCases(t *testing.T) []TestUser {
	return []TestUser{
		{
			name: "on correct expect no errors",
			u: func() *User {
				u := TUser(t)
				return u
			},
			isValid: true,
		},
		{
			name: "on empty username expect an error",
			u: func() *User {
				u := TUser(t)
				u.Username = ""
				return u
			},
			isValid: false,
		},
		{
			name: "on long username expect an error",
			u: func() *User {
				u := TUser(t)
				u.Username = "qwertyqwertyqwertyqwerty"
				return u
			},
			isValid: false,
		},
		{
			name: "on username with spaces expect an error",
			u: func() *User {
				u := TUser(t)
				u.Username = "aaa bbb"
				return u
			},
			isValid: false,
		},
		{
			name: "on latin letters and numbers expect an error",
			u: func() *User {
				u := TUser(t)
				u.Username = "aaa1bbb"
				return u
			},
			isValid: false,
		},
		{
			name: "on cyrillic letters expect an error",
			u: func() *User {
				u := TUser(t)
				u.Username = "йцукен"
				return u
			},
			isValid: false,
		},
	}
}

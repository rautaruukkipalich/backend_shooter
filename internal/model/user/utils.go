package user

import (
	"golang.org/x/crypto/bcrypt"
)

type bcryptHasher struct{}

func (b bcryptHasher) EncryptPassword(password []byte) ([]byte, error) {
	return bcrypt.GenerateFromPassword(password, bcrypt.MinCost)
}

func (b bcryptHasher) ComparePassword(hash, password []byte) error {
	return bcrypt.CompareHashAndPassword(hash, password)
}

package user

import (
	"regexp"
	"time"
)

//go:generate mockery --name PasswordHasher
type PasswordHasher interface {
	EncryptPassword([]byte) ([]byte, error)
	ComparePassword([]byte, []byte) error
}

var Hasher PasswordHasher = bcryptHasher{}

type User struct {
	ID             int64     `json:"id"`
	Username       string    `json:"username"`
	Password       string    `json:"password"`
	HashedPassword string    `json:"hashed_password"`
	CreatedAt      time.Time `json:"created_at"`
	Hasher         PasswordHasher
}

func New(u *User, username string, password string) error {
	u.Username = username
	u.Password = password
	if u.Hasher == nil {
		u.Hasher = bcryptHasher{}
	}

	return u.BeforeCreate()
}

func (u *User) BeforeCreate() error {
	if err := u.Validate(); err != nil {
		return err
	}
	b, err := u.EncryptPassword([]byte(u.Password))
	if err != nil {
		return err
	}
	u.HashedPassword = string(b)
	u.Password = ""

	return nil
}

func (u *User) Validate() error {
	if err := u.ValidateUsername(u.Username); err != nil {
		return err
	}
	if err := u.ValidatePassword(u.Password); err != nil {
		return err
	}
	return nil
}

func (u *User) ValidateUsername(username string) (err error) {
	// check username length
	matched, _ := regexp.MatchString(`^.{3,20}$`, username)
	if !matched {
		return ErrLenUsername
	}
	// check username contains latin letters only
	matched, _ = regexp.MatchString(`^[A-z]+$`, username)
	if !matched {
		return ErrContentUsername
	}

	return nil
}

func (u *User) ValidatePassword(password string) (err error) {

	// check password length (72 symbols - encrypt limit)
	matched, _ := regexp.MatchString(`^.{3,72}$`, password)
	if !matched {
		return ErrLenPassword
	}

	// check password contains letters and digits only
	matched, _ = regexp.MatchString(`^\w*$`, password)
	if !matched {
		return ErrContentPassword
	}

	return nil
}

func (u *User) EncryptPassword(password []byte) ([]byte, error) {
	b, err := u.Hasher.EncryptPassword(password)
	if err != nil {
		return nil, ErrEncryptPassword
	}
	return b, nil
}

func (u *User) ComparePassword(password []byte) bool {
	return u.Hasher.ComparePassword([]byte(u.HashedPassword), password) == nil
}

package character_push_repo

//import (
//	"backend_shooter/internal/database"
//	"backend_shooter/internal/dto"
//	"backend_shooter/internal/model/character"
//)
//
//var tcsCreateCharValidTests = []struct {
//	name   string
//	userID int64
//	form   dto.CreateCharacterRequest
//}{
//	{
//		name:   "Case #1_1 on correct userID and dto.CreateCharacter expect not err",
//		userID: 1,
//		form:   dto.CreateCharacterRequest{Name: "char 1"},
//	},
//	{
//		name:   "Case #1_2 on correct userID and dto.CreateCharacter expect not err",
//		userID: 2,
//		form:   dto.CreateCharacterRequest{Name: "12321321312321"},
//	},
//	{
//		name:   "Case #1_3 on correct userID and dto.CreateCharacter expect not err",
//		userID: 10,
//		form:   dto.CreateCharacterRequest{Name: ";DROP TABLE users;"},
//	},
//}
//
//var tcsCreateCharInvalidTests = []struct {
//	name   string
//	userID int64
//	form   dto.CreateCharacterRequest
//	error  error
//}{
//	{
//		name:   "Case #2_1 on correct userID and empty name in dto.CreateCharacter expect ErrEmptyCharacterName",
//		userID: 1,
//		form:   dto.CreateCharacterRequest{Name: ""},
//		error:  ErrEmptyCharacterName,
//	},
//	{
//		name:   "Case #2_2 on incorrect userID and correct dto.CreateCharacter expect ErrInvalidUserID",
//		userID: -1,
//		form:   dto.CreateCharacterRequest{Name: "123"},
//		error:  ErrInvalidUserID,
//	},
//}
//
//var tcsGetByIDValidTests = []struct {
//	name   string
//	charID int64
//}{
//	{
//		name:   "Case #1_1 on correct charID expect not err",
//		charID: 1,
//	},
//	{
//		name:   "Case #1_2 on correct charID expect not err",
//		charID: 2,
//	},
//}
//
//var tcsGetByIDInvalidTests = []struct {
//	name   string
//	charID int64
//	error  error
//}{
//	{
//		name:   "Case #2_1 on incorrect charID expect ErrCharNotFound",
//		charID: 1,
//		error:  database.ErrCharacterNotFound,
//	},
//	{
//		name:   "Case #2_2 on on incorrect charID expect ErrCharNotFound",
//		charID: 2,
//		error:  database.ErrCharacterNotFound,
//	},
//}
//
//var tcsGetAllValidTests = []struct {
//	name   string
//	userID int64
//	chars  []character.Character
//}{
//	{
//		name:   "Case #1_1 on correct charID expect no error",
//		userID: 1,
//		chars: []character.Character{
//			*character.New("1", 1),
//			*character.New("2", 1),
//		},
//	},
//	{
//		name:   "Case #1_2 on correct charID expect no error",
//		userID: 2,
//		chars: []character.Character{
//			*character.New("1", 2),
//			*character.New("2", 2),
//		},
//	},
//}
//
//var tcsGetAllInvalidTests = []struct {
//	name   string
//	userID int64
//	chars  []character.Character
//}{
//	{
//		name:   "Case #2_1 on incorrect userID expect error and no chars",
//		userID: 1,
//		chars:  []character.Character{},
//	},
//	{
//		name:   "Case #2_2 on incorrect userID expect error and no chars",
//		userID: 2,
//		chars:  []character.Character{},
//	},
//}
//
//var tcsCompareUserAndCharValidTests = []struct {
//	name   string
//	userID int64
//	charID int64
//	res    bool
//	error  error
//}{
//	{
//		name:   "Case #1_1 on correct userID and charID expect no error",
//		userID: 1,
//		charID: 1,
//		res:    true,
//		error:  nil,
//	},
//	{
//		name:   "Case #1_2 on correct userID and charID expect no error",
//		userID: 2,
//		charID: 2,
//		res:    true,
//		error:  nil,
//	},
//}

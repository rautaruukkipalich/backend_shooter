package authservice

import (
	"backend_shooter/internal/database"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model/user"
	"context"
	"errors"
)

type Service struct {
	sql database.UserDB
}

func New(sql database.UserDB) *Service {
	return &Service{sql: sql}
}

func (s *Service) Register(ctx context.Context, r dto.LoginRequest, h user.PasswordHasher) error {
	u := &user.User{Hasher: h}
	if err := user.New(u, r.Username, r.Password); err != nil {
		return err
	}

	ok, err := s.GetUserByUsername(ctx, u)
	if err != nil {
		if !errors.Is(err, database.ErrUserNotFound) {
			return ErrIncorrectUsernameOrPass
		}
	}
	if ok {
		return ErrUserAlreadyExists
	}

	if err := s.sql.User().Create(ctx, u); err != nil {
		return err
	}
	return nil
}

func (s *Service) Login(ctx context.Context, r dto.LoginRequest, h user.PasswordHasher) (int64, error) {
	u := &user.User{Hasher: h}
	if err := user.New(u, r.Username, r.Password); err != nil {
		return 0, ErrIncorrectUsernameOrPass
	}

	ok, err := s.GetUserByUsername(ctx, u)
	if err != nil {
		if errors.Is(err, database.ErrUserNotFound) || !ok {
			return 0, ErrIncorrectUsernameOrPass
		}
		return 0, ErrIncorrectUsernameOrPass
	}

	if !u.ComparePassword([]byte(r.Password)) {
		return 0, ErrIncorrectUsernameOrPass
	}

	return u.ID, nil
}

func (s *Service) GetUserByUsername(ctx context.Context, u *user.User) (bool, error) {
	return s.sql.User().GetByUsername(ctx, u.Username, u)
}

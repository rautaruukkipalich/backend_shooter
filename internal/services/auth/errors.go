package authservice

import "errors"

var (
	ErrUserAlreadyExists       = errors.New("user already exists")
	ErrIncorrectUsernameOrPass = errors.New("incorrect username or password")
)

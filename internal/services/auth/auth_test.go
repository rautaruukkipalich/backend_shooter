package authservice

import (
	"backend_shooter/internal/database"
	dbMocks "backend_shooter/internal/database/mocks"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model/user"
	userMocks "backend_shooter/internal/model/user/mocks"
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"log"
	"testing"
)

func Test_Auth_Register(t *testing.T) {

	type tcsRegister struct {
		name string
		form dto.LoginRequest
		err  error
	}

	tcsRegisterValidTests := []tcsRegister{
		{
			name: "Case #1_1 on correct dto.LoginForm expect not err",
			form: dto.LoginRequest{
				Username: "admin",
				Password: "admin",
			},
			err: nil,
		},
		{
			name: "Case #1_2 on correct dto.LoginForm expect not err",
			form: dto.LoginRequest{
				Username: "user",
				Password: "user",
			},
			err: nil,
		},
	}

	for _, tc := range tcsRegisterValidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()

			storeMock := new(dbMocks.UserDB)
			storeUserMock := new(dbMocks.UserRepo)

			hasher := new(userMocks.PasswordHasher)

			hasher.On("EncryptPassword", []byte(tc.form.Password)).Return([]byte(tc.form.Password), nil)
			//hasher.On("ComparePassword", []byte(tc.form.Password), []byte(tc.form.Password)).Return(true)

			u := user.User{Hasher: hasher}
			err := user.New(&u, tc.form.Username, tc.form.Password)
			if err != nil {
				log.Fatal(err)
			}

			storeMock.On("User").Return(storeUserMock)

			storeUserMock.On("GetByUsername", ctx, u.Username, &u).Return(false, database.ErrUserNotFound)
			storeUserMock.On("Create", ctx, &u).Return(nil)

			service := Service{sql: storeMock}
			err = service.Register(ctx, tc.form, hasher)

			assert.NoError(t, err)
			storeMock.AssertExpectations(t)
			storeUserMock.AssertExpectations(t)
		})
	}

	var tcsRegisterInvalidTests = []tcsRegister{
		{
			name: "Case #2_1 on incorrect dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: " ",
				Password: "user",
			},
			err: user.ErrLenUsername,
		},
		{
			name: "Case #2_2 on incorrect dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "asdsadsa",
				Password: "1",
			},
			err: user.ErrLenPassword,
		},
		{
			name: "Case #2_3 on incorrect dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "123213",
				Password: "user",
			},
			err: user.ErrContentUsername,
		},
		{
			name: "Case #2_4 on incorrect dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "asdsadsa",
				Password: "1321.,321",
			},
			err: user.ErrContentPassword,
		},
	}

	for _, tc := range tcsRegisterInvalidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()
			storeMock := new(dbMocks.UserDB)

			hasher := new(userMocks.PasswordHasher)

			hasher.On("EncryptPassword", []byte(tc.form.Password)).Return([]byte(tc.form.Password), nil)
			hasher.On("ComparePassword", []byte(tc.form.Password), []byte(tc.form.Password)).Return(true)

			service := Service{sql: storeMock}

			err := service.Register(ctx, tc.form, hasher)

			assert.Error(t, err)
			assert.Equal(t, tc.err, err)
			storeMock.AssertNotCalled(t, "User", mock.Anything)
		})
	}
}

func Test_Auth_Login(t *testing.T) {
	type tcsLogin struct {
		name string
		form dto.LoginRequest
		err  error
	}

	tcsLoginValidTests := []tcsLogin{
		{
			name: "Case #1_1 on correct dto.LoginForm expect no err",
			form: dto.LoginRequest{
				Username: "admin",
				Password: "admin",
			},
			err: nil,
		},
		{
			name: "Case #1_2 on correct dto.LoginForm expect no err",
			form: dto.LoginRequest{
				Username: "user",
				Password: "user",
			},
			err: nil,
		},
	}

	for _, tc := range tcsLoginValidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()
			storeMock := new(dbMocks.UserDB)
			storeUserMock := new(dbMocks.UserRepo)
			hasher := new(userMocks.PasswordHasher)

			hasher.On("EncryptPassword", []byte(tc.form.Password)).Return([]byte(tc.form.Password), nil)
			hasher.On("ComparePassword", []byte(tc.form.Password), []byte(tc.form.Password)).Return(nil)

			u := user.User{Hasher: hasher}
			err := user.New(&u, tc.form.Username, tc.form.Password)
			if err != nil {
				log.Fatal(err)
			}

			storeMock.On("User").Return(storeUserMock)
			storeUserMock.On("GetByUsername", ctx, tc.form.Username, &u).Return(true, nil)

			service := Service{sql: storeMock}
			_, err = service.Login(ctx, tc.form, hasher)

			assert.NoError(t, err)
			hasher.AssertExpectations(t)
			storeMock.AssertExpectations(t)
			storeUserMock.AssertExpectations(t)
		})
	}

	tcsLoginInvalidTests := []tcsLogin{
		{
			name: "Case #2_1 on incorrect dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "admin",
				Password: "admin1",
			},
			err: ErrIncorrectUsernameOrPass,
		},
		{
			name: "Case #2_2 on incorrect dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "user",
				Password: "user2",
			},
			err: ErrIncorrectUsernameOrPass,
		},
	}

	for _, tc := range tcsLoginInvalidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.Background()
			storeMock := new(dbMocks.UserDB)
			storeUserMock := new(dbMocks.UserRepo)
			hasher := new(userMocks.PasswordHasher)

			hasher.On("EncryptPassword", []byte(tc.form.Password)).Return([]byte(tc.form.Password), nil)
			hasher.On("ComparePassword", []byte(tc.form.Password), []byte(tc.form.Password)).Return(ErrIncorrectUsernameOrPass)

			u := user.User{Hasher: hasher}
			err := user.New(&u, tc.form.Username, tc.form.Password)
			if err != nil {
				log.Fatal(err)
			}

			storeMock.On("User").Return(storeUserMock)
			storeUserMock.On("GetByUsername", ctx, tc.form.Username, &u).Return(true, nil)

			service := Service{sql: storeMock}
			uid, err := service.Login(ctx, tc.form, hasher)

			assert.Error(t, err)
			assert.Equal(t, int64(0), uid)
			hasher.AssertExpectations(t)
			storeMock.AssertExpectations(t)
			storeUserMock.AssertExpectations(t)
		})
	}

	var tcsLoginInvalidTests2 = []tcsLogin{
		{
			name: "Case #3_1 on short username in dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: " ",
				Password: "user",
			},
			err: ErrIncorrectUsernameOrPass,
		},
		{
			name: "Case #3_2 on short password dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "asdsadsa",
				Password: "1",
			},
			err: ErrIncorrectUsernameOrPass,
		},
		{
			name: "Case #3_3 on incorrect username dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "123213",
				Password: "user",
			},
			err: ErrIncorrectUsernameOrPass,
		},
		{
			name: "Case #3_4 on incorrect password dto.LoginForm expect err",
			form: dto.LoginRequest{
				Username: "asdsadsa",
				Password: "1321.,321",
			},
			err: ErrIncorrectUsernameOrPass,
		},
	}

	for _, tc := range tcsLoginInvalidTests2 {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()
			storeMock := new(dbMocks.UserDB)
			hasher := new(userMocks.PasswordHasher)

			service := Service{sql: storeMock}

			uid, err := service.Login(ctx, tc.form, hasher)

			assert.Error(t, err)
			assert.Equal(t, tc.err, err)
			assert.Equal(t, int64(0), uid)
			storeMock.AssertNotCalled(t, "User", mock.Anything)
			hasher.AssertNotCalled(t, "ComparePassword", mock.Anything)
		})
	}
}

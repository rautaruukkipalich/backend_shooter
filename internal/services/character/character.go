package characterservice

import (
	"backend_shooter/internal/database"
	"backend_shooter/internal/dto"
	"backend_shooter/internal/model"
	"backend_shooter/internal/model/character"
	"backend_shooter/internal/services"
	"context"
	"log"
)

type Service struct {
	sql  database.CharacterDB
	fast database.CharacterFastDB
}

func New(sql database.CharacterDB, fast database.CharacterFastDB) *Service {
	return &Service{sql: sql, fast: fast}
}

func (s *Service) Save(ctx context.Context, char character.Character) {
	if err := s.sql.Character().Save(ctx, char); err != nil {
		log.Println(err)
	}
}

func (s *Service) Create(ctx context.Context, userID int64, form dto.CreateCharacterRequest) error {

	if len(form.Name) == services.ZeroValue {
		return ErrEmptyCharacterName
	}

	if userID <= services.ZeroValue {
		return ErrInvalidUserID
	}

	newChar := character.New(form.Name, userID)
	if err := s.sql.Character().Create(ctx, newChar); err != nil {
		return err
	}

	return nil
}

func (s *Service) GetByID(ctx context.Context, charID int64, char *character.Character) error {
	ok, err := s.sql.Character().GetByID(ctx, charID, char)
	if err != nil || !ok {
		return ErrNotFoundCharacter
	}
	return nil
}

func (s *Service) GetCharactersByUserID(ctx context.Context, userID int64) (dto.CharactersResponse, error) {
	chars, err := s.sql.Character().GetByUserID(ctx, userID)
	if err != nil {
		return dto.CharactersResponse{}, err
	}
	return dto.CharactersResponse{Characters: chars}, nil
}

func (s *Service) CompareCharAndUser(ctx context.Context, userID, charID int64) (bool, error) {
	var char character.Character
	err := s.GetByID(ctx, charID, &char)
	if err != nil {
		return false, err
	}
	return char.UserID == userID, nil
}

func (s *Service) ArenaGetAll(ctx context.Context, char *character.Character) []character.Character {
	return s.fast.Character().ArenaGetAll(ctx, char)
}

func (s *Service) GetHead(ctx context.Context) *model.Steps {
	return s.fast.Character().GetHead(ctx)
}

func (s *Service) Connect(ctx context.Context, char *character.Character) *character.Character {
	return s.fast.Character().Connect(ctx, char)
}

func (s *Service) Disconnect(ctx context.Context, char *character.Character) {
	s.fast.Character().Disconnect(ctx, char)
}

func (s *Service) Update(ctx context.Context, char *character.Character, step []byte) {
	s.fast.Character().Update(ctx, char, step)
}

func (s *Service) Wait() {
	s.fast.Character().Wait()
}

package characterservice

import (
	"backend_shooter/internal/database"
	"backend_shooter/internal/database/mocks"
	"backend_shooter/internal/model/character"
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

func Test_Character_Create(t *testing.T) {

	for _, tc := range tcsCreateCharValidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()

			storeMock := new(mocks.CharacterDB)
			storeCharacterMock := new(mocks.CharacterRepo)

			char := character.New(tc.form.Name, tc.userID)
			storeMock.On("Character").Return(storeCharacterMock)
			storeCharacterMock.On("Create", ctx, char).Return(nil)

			service := Service{sql: storeMock}
			err := service.Create(ctx, tc.userID, tc.form)

			assert.NoError(t, err)
			storeMock.AssertExpectations(t)
			storeCharacterMock.AssertExpectations(t)
		})
	}

	for _, tc := range tcsCreateCharInvalidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()

			storeMock := new(mocks.CharacterDB)

			service := Service{sql: storeMock}
			err := service.Create(ctx, tc.userID, tc.form)

			assert.Error(t, err)
			assert.Equal(t, tc.error, err)

			storeMock.AssertNotCalled(t, "Character", mock.Anything)
		})
	}
}

func Test_Character_GetByID(t *testing.T) {
	for _, tc := range tcsGetByIDValidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()

			storeMock := new(mocks.CharacterDB)
			storeCharacterMock := new(mocks.CharacterRepo)

			char := character.New("char", 1)
			storeMock.On("Character").Return(storeCharacterMock)
			storeCharacterMock.On("GetByID", ctx, tc.charID, char).Return(true, nil)

			service := Service{sql: storeMock}
			err := service.GetByID(ctx, tc.charID, char)

			assert.NoError(t, err)
			storeMock.AssertExpectations(t)
			storeCharacterMock.AssertExpectations(t)
		})
	}

	for _, tc := range tcsGetByIDInvalidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()

			storeMock := new(mocks.CharacterDB)
			storeCharacterMock := new(mocks.CharacterRepo)

			char := character.New("char", 1)
			storeMock.On("Character").Return(storeCharacterMock)
			storeCharacterMock.On("GetByID", ctx, tc.charID, char).Return(false, tc.error)

			service := Service{sql: storeMock}
			err := service.GetByID(ctx, tc.charID, char)

			assert.Error(t, err)
			storeMock.AssertExpectations(t)
			storeCharacterMock.AssertExpectations(t)
		})
	}
}

func Test_Character_GetAll(t *testing.T) {
	for _, tc := range tcsGetAllValidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()

			storeMock := new(mocks.CharacterDB)
			storeCharacterMock := new(mocks.CharacterRepo)

			storeMock.On("Character").Return(storeCharacterMock)
			storeCharacterMock.On("GetByUserID", ctx, tc.userID).Return(tc.chars, nil)

			service := Service{sql: storeMock}
			chars, err := service.GetCharactersByUserID(ctx, tc.userID)

			assert.NoError(t, err)
			assert.GreaterOrEqual(t, len(chars.Characters), 1)
			storeMock.AssertExpectations(t)
			storeCharacterMock.AssertExpectations(t)
		})
	}

	for _, tc := range tcsGetAllInvalidTests {
		t.Run(tc.name, func(t *testing.T) {
			ctx := context.TODO()

			storeMock := new(mocks.CharacterDB)
			storeCharacterMock := new(mocks.CharacterRepo)

			storeMock.On("Character").Return(storeCharacterMock)
			storeCharacterMock.On("GetByUserID", ctx, tc.userID).Return(tc.chars, database.ErrNoRows)

			service := Service{sql: storeMock}
			chars, err := service.GetCharactersByUserID(ctx, tc.userID)

			assert.Error(t, err)
			assert.Equal(t, len(chars.Characters), 0)
			storeMock.AssertExpectations(t)
			storeCharacterMock.AssertExpectations(t)
		})
	}
}

func Test_Character_CompareCharAndUser(t *testing.T) {
	for _, tc := range tcsCompareUserAndCharValidTests {
		t.Run(tc.name, func(t *testing.T) {
		})
	}
}

//func Test_Character_CompareCharAndUser(t *testing.T) {
//	for _, tc := range tcsCompareUserAndCharValidTests {
//		t.Run(tc.name, func(t *testing.T) {
//			ctx := context.Background()
//
//			storeMock := new(mocks.CharacterDB)
//			storeCharacterMock := new(mocks.CharacterRepo)
//
//			char := character.New("123", tc.userID)
//			char.ID = tc.charID
//
//			storeMock.On("Character").Return(storeCharacterMock)
//			storeCharacterMock.On("GetByID", ctx, tc.charID, char).Return(true, nil)
//
//			service := Service{sql: storeMock}
//			ok, err := service.CompareCharAndUser(ctx, tc.userID, tc.charID)
//
//			assert.NoError(t, err)
//			assert.Equal(t, tc.res, ok)
//			storeMock.AssertExpectations(t)
//			storeCharacterMock.AssertExpectations(t)
//		})
//	}
//}

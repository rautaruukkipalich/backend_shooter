package character_pull_repo

import "errors"

var (
	ErrEmptyCharacterName = errors.New("empty character name")
	ErrNotFoundCharacter  = errors.New("character not found")
	ErrInvalidUserID      = errors.New("invalid user id")
)

FROM golang:1.22.4-alpine AS builder

RUN apk update && apk --no-cache add bash git make

WORKDIR /usr/src

COPY ["go.mod","go.sum","./"]

RUN go mod download

COPY . .

# build
RUN go build -tags goexperiment.arenas -o ./bin/app ./cmd/app/main.go

# create alpine
FROM alpine:latest AS runner

RUN apk update

# copy binary from builder
COPY --from=builder /usr/src/bin/app .
